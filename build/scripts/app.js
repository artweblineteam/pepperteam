(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _video = require('./app/video');

var _video2 = _interopRequireDefault(_video);

var _project = require('./app/project');

var _project2 = _interopRequireDefault(_project);

var _card = require('./app/card');

var _card2 = _interopRequireDefault(_card);

var _scan = require('./app/scan');

var _scan2 = _interopRequireDefault(_scan);

var _scroller = require('./app/scroller');

var _scroller2 = _interopRequireDefault(_scroller);

var _dot = require('./app/dot');

var _dot2 = _interopRequireDefault(_dot);

var _closer = require('./app/closer');

var _closer2 = _interopRequireDefault(_closer);

var _scrollingText = require('./app/scrollingText');

var _scrollingText2 = _interopRequireDefault(_scrollingText);

var _projectVideo = require('./app/projectVideo');

var _projectVideo2 = _interopRequireDefault(_projectVideo);

var _portfolioSelector = require('./app/portfolioSelector');

var _portfolioSelector2 = _interopRequireDefault(_portfolioSelector);

var _portfolio = require('./app/portfolio');

var _portfolio2 = _interopRequireDefault(_portfolio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
  window.isMobile = true;
  document.documentElement.classList.add('is__mobile');

  var setOrientation = function setOrientation() {
    if (window.innerHeight < window.innerWidth) {
      document.documentElement.classList.add('is__horizontal');
    } else {
      document.documentElement.classList.remove('is__horizontal');
    }
  };

  window.addEventListener('orientationchange', setOrientation);
  setOrientation();
}

document.documentElement.classList.add('is__ready');

if (location.pathname.split('/').filter(Boolean).pop() === 'en') {
  var links = [].slice.call(document.querySelectorAll('.portfolioLink-link, .logo-dot-link'));
  links.forEach(function (link) {
    return link.href && (link.href = 'en/' + link.href);
  });
}
//import Fotorama from './app/fotorama'


{
  document.querySelectorAll('.js__video').forEach(function (el) {
    return new _video2.default(el);
  });
  document.querySelectorAll('.js__project').forEach(function (el) {
    return new _project2.default(el);
  });
  document.querySelectorAll('.js__card').forEach(function (el) {
    return new _card2.default(el);
  });
  document.querySelectorAll('.js__scan').forEach(function (el) {
    return new _scan2.default(el);
  });
  document.querySelectorAll('.js__scroller').forEach(function (el) {
    return new _scroller2.default(el);
  });
  //document.querySelectorAll('.js_slickslider').forEach(el => new SlickSlider(el))
  document.querySelectorAll('.js__dot').forEach(function (el) {
    return new _dot2.default(el);
  });
  document.querySelectorAll('.js__closer').forEach(function (el) {
    return new _closer2.default(el);
  });
  document.querySelectorAll('.js__scrollingText').forEach(function (el) {
    return new _scrollingText2.default(el);
  });
  document.querySelectorAll('.js__projectVideo').forEach(function (el) {
    return new _projectVideo2.default(el);
  });
  document.querySelectorAll('.js__portfolioSelector').forEach(function (el) {
    return new _portfolioSelector2.default(el);
  });
  document.querySelectorAll('.js__portfolio').forEach(function (el) {
    return new _portfolio2.default(el);
  });
}

$(function () {
  $('.smart-shaft').fotorama();
});

$(function () {
  $('.team-carousel').fotorama();
});

// $(function () {
//   $('.team-carousel').owlCarousel({
//     loop:true,
//     margin:10,
//     nav:true,
// });
// });

},{"./app/card":2,"./app/closer":3,"./app/dot":4,"./app/portfolio":5,"./app/portfolioSelector":6,"./app/project":7,"./app/projectVideo":8,"./app/scan":9,"./app/scroller":10,"./app/scrollingText":11,"./app/video":12}],2:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Card = function () {
  function Card($el) {
    _classCallCheck(this, Card);

    this.$el = $el;
    this.target = document.querySelector(this.$el.getAttribute('href'));

    this._bindEvents();
  }

  _createClass(Card, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      var _this = this;

      this.$el.addEventListener('click', function (e) {
        e.preventDefault();
        _this.target.dispatchEvent(new Event('open'));
      });
    }
  }]);

  return Card;
}();

module.exports = Card;

},{}],3:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Closer = function () {
  function Closer($el) {
    _classCallCheck(this, Closer);

    this.$el = $el;

    this._bindEvents();
  }

  _createClass(Closer, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      this.$el.addEventListener('click', function (e) {
        e.stopPropagation();

        document.dispatchEvent(new CustomEvent('closePopups'));
      });
    }
  }]);

  return Closer;
}();

module.exports = Closer;

},{}],4:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CLASS_ACTIVE = 'is__active';
var CLASS_POPUP = 'has__popup';

var Dot = function () {
  function Dot($el) {
    _classCallCheck(this, Dot);

    this.$el = $el;

    this._bindEvents();
  }

  _createClass(Dot, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      var _this = this;

      this.$el.addEventListener('click', function (e) {
        e.stopPropagation();
        document.body.classList.add(CLASS_POPUP);
        _this.$el.classList.add(CLASS_ACTIVE);

        document.dispatchEvent(new CustomEvent('closePopups', { detail: { $el: _this.$el } }));
      });

      document.addEventListener('click', function (_) {
        _this.$el.classList.remove(CLASS_ACTIVE);
        document.body.classList.remove(CLASS_POPUP);
      });

      document.addEventListener('closePopups', function (e, data) {
        if (!e.detail || e.detail.$el !== _this.$el) {
          _this.$el.classList.remove(CLASS_ACTIVE);
        }

        if (!e.detail) {
          document.body.classList.remove(CLASS_POPUP);
        }
      });
    }
  }]);

  return Dot;
}();

module.exports = Dot;

},{}],5:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Portfolio = function () {
  function Portfolio($el) {
    _classCallCheck(this, Portfolio);

    this.$el = $el;
    this.$back = $el.querySelectorAll('.js__portfolio-back');

    this.baseTitle = document.title;

    if (window.location.hash) {
      this._hideSelector();

      if (window.location.hash === 'photo' || window.location.hash === 'video') {
        this._filter(window.location.hash);
      }
    }

    this._bindEvents();
  }

  _createClass(Portfolio, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      var _this = this;

      document.addEventListener('portfolioFilter', function (e) {
        _this._filter(e.detail);
      });

      this.$back.forEach(function ($back) {
        $back.addEventListener('click', function (e) {
          e.preventDefault();

          _this._filter();
        });
      });
    }
  }, {
    key: '_filter',
    value: function _filter(type) {
      if (type) {
        history.pushState(null, type, '#' + type);
        window.prevType = type;
        this._clear();
        this.$el.classList.add('is__' + type);
        this.currentType = type;
        this._hideSelector();
      } else {
        history.pushState(null, this.baseTitle, window.location.pathname);
        delete window.prevType;
        this._clear();
        this._showSelector();
      }
    }
  }, {
    key: '_hideSelector',
    value: function _hideSelector() {
      this.$el.classList.add('is__hiddenSelector');
    }
  }, {
    key: '_showSelector',
    value: function _showSelector() {
      this.$el.classList.remove('is__hiddenSelector');
    }
  }, {
    key: '_clear',
    value: function _clear() {
      this.$el.classList.remove('is__' + this.currentType);
    }
  }]);

  return Portfolio;
}();

module.exports = Portfolio;

},{}],6:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PortfolioSelector = function () {
  function PortfolioSelector($el) {
    _classCallCheck(this, PortfolioSelector);

    this.$el = $el;
    this.target = this.$el.getAttribute('href').slice(1);

    this._bindEvents();
  }

  _createClass(PortfolioSelector, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      var _this = this;

      this.$el.addEventListener('click', function (e) {
        e.preventDefault();
        document.dispatchEvent(new CustomEvent('portfolioFilter', { 'detail': _this.target }));
      });
    }
  }]);

  return PortfolioSelector;
}();

module.exports = PortfolioSelector;

},{}],7:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CLASS_OPENED = 'is__opened';
var CLASS_FADED = 'is__faded';

var Project = function () {
  function Project($el) {
    _classCallCheck(this, Project);

    this.$el = $el;
    this.$body = document.body;
    this.$window = window;
    this.$content = this.$el.children[0];
    this.$video = this.$content.querySelector('video');
    this.$close = this.$content.querySelector('.js__project-close');

    this.baseTitle = document.title;
    this.title = this.$el.querySelector('h2').innerText;

    this.id = this.$el.id;

    if (window.location.hash === '#' + this.id) {
      this._open();
    }

    this._bindEvents();
  }

  _createClass(Project, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      var _this = this;

      this.$el.addEventListener('open', this._open.bind(this));
      this.$el.addEventListener('close', this._close.bind(this));
      this.$el.addEventListener('click', this._close.bind(this));
      this.$close.addEventListener('click', this._close.bind(this));
      this.$content.childNodes.forEach(function (child) {
        child.addEventListener('click', function (e) {
          return e.stopPropagation();
        });
      });
      document.addEventListener('keydown', function (e) {
        if (e.keyCode === 27) _this._close();
      });
    }
  }, {
    key: '_open',
    value: function _open() {
      console.log("open");
      this.$el.classList.add(CLASS_OPENED);
      this.$body.classList.add(CLASS_FADED);
      document.title = this.title;
      this._attachWheel.bind(this);

      if (this.$video && !window.isMobile) {
        this.$video.play();
      }

      history.pushState(null, this.title, '#' + this.id);
    }
  }, {
    key: '_close',
    value: function _close() {
      console.log("close");
      this.$el.classList.remove(CLASS_OPENED);
      this.$body.classList.remove(CLASS_FADED);
      document.title = this.baseTitle;
      history.pushState(null, this.baseTitle, window.location.pathname);
      this._removeWheel.bind(this);

      if (this.$video && !window.isMobile) {
        this.$video.pause();
      }

      document.dispatchEvent(new CustomEvent('portfolioFilter', { 'detail': window.prevType }));
    }
  }, {
    key: '_attachWheel',
    value: function _attachWheel() {
      console.log('_attachWheel', this);
      if (this.$window.addEventListener) {
        if ('onwheel' in document) {
          // IE9+, FF17+, Ch31+
          this.$window.addEventListener("wheel", this._scroll.bind(this));
        } else if ('onmousewheel' in document) {
          // устаревший вариант события
          this.$window.addEventListener("mousewheel", this._scroll.bind(this));
        } else {
          // Firefox < 17
          this.$window.addEventListener("MozMousePixelScroll", this._scroll.bind(this));
        }
      } else {
        // IE8-
        this.$window.attachEvent("onmousewheel", this._scroll.bind(this));
      }
    }
  }, {
    key: '_removeWheel',
    value: function _removeWheel() {
      console.log('_removeWheel', this);
      if (this.$window.removeEventListener) {
        if ('onwheel' in document) {
          // IE9+, FF17+, Ch31+
          this.$window.removeEventListener("wheel", this._scroll.bind(this));
        } else if ('onmousewheel' in document) {
          // устаревший вариант события
          this.$window.removeEventListener("mousewheel", this._scroll.bind(this));
        } else {
          // Firefox < 17
          this.$window.removeEventListener("MozMousePixelScroll", this._scroll.bind(this));
        }
      } else {
        // IE8-
        this.$window.detachEvent("onmousewheel", this._scroll.bind(this));
      }
    }
  }, {
    key: '_scroll',
    value: function _scroll(e) {
      console.log('scroll');
      function handle(delta, target) {
        var top = target.scrollTop() - delta;
        target.scrollTop(top);
      }

      e = e || window.event;

      var delta = e.deltaY || e.detail || e.wheelDelta;

      handle(delta, this.$el);

      e.preventDefault ? e.preventDefault() : e.returnValue = false;
    }
  }]);

  return Project;
}();

module.exports = Project;

},{}],8:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ProjectVideo = function () {
  function ProjectVideo($el) {
    _classCallCheck(this, ProjectVideo);

    this.$el = $el;

    this.isPlaying = true;

    if (window.isMobile) {
      this.$el.setAttribute('controls', 'controls');
    }

    this._bindEvents();
  }

  _createClass(ProjectVideo, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      this.$el.addEventListener('mouseover', this._showControls.bind(this));
      this.$el.addEventListener('mouseout', this._hideControls.bind(this));
      this.$el.addEventListener('click', this._playPause.bind(this));
    }
  }, {
    key: '_playPause',
    value: function _playPause() {
      this.isPlaying ? this.$el.pause() : this.$el.play();
      this.isPlaying = !this.isPlaying;
    }
  }, {
    key: '_showControls',
    value: function _showControls() {
      this.$el.controls = true;
    }
  }, {
    key: '_hideControls',
    value: function _hideControls() {
      this.$el.controls = false;
    }
  }]);

  return ProjectVideo;
}();

module.exports = ProjectVideo;

},{}],9:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CLASS_SCANNED = 'is__scanned';
var CLASS_CLONE = 'scan-clone';
var CLONES_NUMBER = 5;

var Card = function () {
  function Card($el) {
    _classCallCheck(this, Card);

    this.$el = $el;

    this._buildClones();
  }

  _createClass(Card, [{
    key: '_buildClones',
    value: function _buildClones() {
      var text = this.$el.innerText;
      for (var i = 0; i < CLONES_NUMBER; i++) {
        var clone = this.$el.cloneNode();
        clone.removeAttribute('id');
        clone.classList.remove('scan');
        clone.classList.remove('js__scan');
        clone.classList.add(CLASS_CLONE);
        clone.innerText = text;
        this.$el.appendChild(clone);
      }
    }
  }]);

  return Card;
}();

module.exports = Card;

},{}],10:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Scroller = function () {
  function Scroller($el) {
    _classCallCheck(this, Scroller);

    this.$el = $el;

    this._bindEvents();
  }

  _createClass(Scroller, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      var _this = this;

      this.$el.addEventListener('wheel', function (e) {
        _this.$el.scrollTop -= e.wheelDeltaY;
        e.preventDefault();
      });
    }
  }]);

  return Scroller;
}();

module.exports = Scroller;

},{}],11:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var RESIZE_EVENT = window.isMobile ? 'orientationchange' : 'reize';
var CLONES_NUMBER = 5;

var ScrollingText = function () {
  function ScrollingText($el) {
    _classCallCheck(this, ScrollingText);

    this.$el = $el;
    this.$in = this.$el.firstElementChild;
    this.$text = this.$in.firstElementChild;

    this.clones = [];
    this.duration = this.$el.dataset.duration * CLONES_NUMBER;
    this.isInversed = this.$el.dataset.inverse;

    this._buildStyleTag();
    this._bindEvents();
    this._init();
  }

  _createClass(ScrollingText, [{
    key: '_buildStyleTag',
    value: function _buildStyleTag() {
      this.$style = document.createElement('style');
      document.head.appendChild(this.$style);
    }
  }, {
    key: '_init',
    value: function _init() {
      var windowWidth = window.innerWidth;
      this.width = this.$in.clientWidth;

      if (this.width) {
        var clonesNumber = Math.round(windowWidth / this.width) + 1;
        this.overScroll = clonesNumber * this.width - windowWidth;

        for (var i = 0; i < clonesNumber; i++) {
          var $clone = this.$text.cloneNode(true);

          this.clones.push($clone);
          this.$in.appendChild($clone);
        }

        this.hwidth = this.$in.clientWidth * CLONES_NUMBER;

        for (var _i = 0; _i < CLONES_NUMBER; _i++) {
          var _$clone = this.$in.cloneNode(true);
          this.clones.push(_$clone);
          this.$el.appendChild(_$clone);
        }
      }

      this._buildKeyframe();
    }
  }, {
    key: '_buildKeyframe',
    value: function _buildKeyframe() {
      var id = 'id' + new Date().getTime();

      this.$el.id = id;

      var from = this.isInversed ? -this.hwidth : 0;
      var till = this.isInversed ? 0 : -this.hwidth;

      var style = '\n      @keyframes scroll-' + id + ' {\n       0% { transform: translateX(' + from + 'px) translateZ(0); }\n       100% { transform: translateX(' + till + 'px) translateZ(0); }\n      }\n\n      #' + id + ' {\n        animation: scroll-' + id + ' ' + this.duration + 's linear infinite;\n      }\n    ';

      this.$style.appendChild(document.createTextNode(style));
    }
  }, {
    key: '_reinit',
    value: function _reinit() {
      this._removeClones();
      this._init();
    }
  }, {
    key: '_removeClones',
    value: function _removeClones() {
      this.clones.forEach(function ($clone) {
        return $clone.remove();
      });
      this.clones = [];
    }
  }, {
    key: '_bindEvents',
    value: function _bindEvents() {
      window.addEventListener(RESIZE_EVENT, this._reinit.bind(this));
    }
  }]);

  return ScrollingText;
}();

module.exports = ScrollingText;

},{}],12:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CLASS_PLAYING = 'is__playing';

var Video = function () {
  function Video($el) {
    _classCallCheck(this, Video);

    this.$el = $el;

    this._bindEvents();
  }

  _createClass(Video, [{
    key: '_bindEvents',
    value: function _bindEvents() {
      this.$el.addEventListener('mouseover', this._play.bind(this));
      this.$el.addEventListener('mouseout', this._pause.bind(this));
      this.$el.addEventListener('click', function (e) {
        return e.preventDefault();
      });
    }
  }, {
    key: '_play',
    value: function _play() {
      this.$el.play();
      this.$el.classList.add(CLASS_PLAYING);
    }
  }, {
    key: '_pause',
    value: function _pause() {
      this.$el.pause();
      this.$el.classList.remove(CLASS_PLAYING);
    }
  }]);

  return Video;
}();

module.exports = Video;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9hcHAuanMiLCJzb3VyY2Uvc2NyaXB0cy9hcHAvY2FyZC5qcyIsInNvdXJjZS9zY3JpcHRzL2FwcC9jbG9zZXIuanMiLCJzb3VyY2Uvc2NyaXB0cy9hcHAvZG90LmpzIiwic291cmNlL3NjcmlwdHMvYXBwL3BvcnRmb2xpby5qcyIsInNvdXJjZS9zY3JpcHRzL2FwcC9wb3J0Zm9saW9TZWxlY3Rvci5qcyIsInNvdXJjZS9zY3JpcHRzL2FwcC9wcm9qZWN0LmpzIiwic291cmNlL3NjcmlwdHMvYXBwL3Byb2plY3RWaWRlby5qcyIsInNvdXJjZS9zY3JpcHRzL2FwcC9zY2FuLmpzIiwic291cmNlL3NjcmlwdHMvYXBwL3Njcm9sbGVyLmpzIiwic291cmNlL3NjcmlwdHMvYXBwL3Njcm9sbGluZ1RleHQuanMiLCJzb3VyY2Uvc2NyaXB0cy9hcHAvdmlkZW8uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ3lCQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFwQ0EsSUFBRyxpRUFBaUUsSUFBakUsQ0FBc0UsVUFBVSxTQUFoRixDQUFILEVBQStGO0FBQzdGLFNBQU8sUUFBUCxHQUFrQixJQUFsQjtBQUNBLFdBQVMsZUFBVCxDQUF5QixTQUF6QixDQUFtQyxHQUFuQyxDQUF1QyxZQUF2Qzs7QUFFQSxNQUFNLGlCQUFpQixTQUFqQixjQUFpQixHQUFNO0FBQzNCLFFBQUksT0FBTyxXQUFQLEdBQXFCLE9BQU8sVUFBaEMsRUFBNEM7QUFDMUMsZUFBUyxlQUFULENBQXlCLFNBQXpCLENBQW1DLEdBQW5DLENBQXVDLGdCQUF2QztBQUNELEtBRkQsTUFFTztBQUNMLGVBQVMsZUFBVCxDQUF5QixTQUF6QixDQUFtQyxNQUFuQyxDQUEwQyxnQkFBMUM7QUFDRDtBQUNGLEdBTkQ7O0FBUUEsU0FBTyxnQkFBUCxDQUF3QixtQkFBeEIsRUFBNkMsY0FBN0M7QUFDQTtBQUNEOztBQUVELFNBQVMsZUFBVCxDQUF5QixTQUF6QixDQUFtQyxHQUFuQyxDQUF1QyxXQUF2Qzs7QUFFQSxJQUFJLFNBQVMsUUFBVCxDQUFrQixLQUFsQixDQUF3QixHQUF4QixFQUE2QixNQUE3QixDQUFvQyxPQUFwQyxFQUE2QyxHQUE3QyxPQUF1RCxJQUEzRCxFQUFpRTtBQUMvRCxNQUFNLFFBQVEsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLFNBQVMsZ0JBQVQsQ0FBMEIscUNBQTFCLENBQWQsQ0FBZDtBQUNBLFFBQU0sT0FBTixDQUFjO0FBQUEsV0FBUSxLQUFLLElBQUwsS0FBYyxLQUFLLElBQUwsR0FBWSxRQUFRLEtBQUssSUFBdkMsQ0FBUjtBQUFBLEdBQWQ7QUFDRDtBQVNEOzs7QUFRQTtBQUNFLFdBQVMsZ0JBQVQsQ0FBMEIsWUFBMUIsRUFBd0MsT0FBeEMsQ0FBZ0Q7QUFBQSxXQUFNLG9CQUFVLEVBQVYsQ0FBTjtBQUFBLEdBQWhEO0FBQ0EsV0FBUyxnQkFBVCxDQUEwQixjQUExQixFQUEwQyxPQUExQyxDQUFrRDtBQUFBLFdBQU0sc0JBQVksRUFBWixDQUFOO0FBQUEsR0FBbEQ7QUFDQSxXQUFTLGdCQUFULENBQTBCLFdBQTFCLEVBQXVDLE9BQXZDLENBQStDO0FBQUEsV0FBTSxtQkFBUyxFQUFULENBQU47QUFBQSxHQUEvQztBQUNBLFdBQVMsZ0JBQVQsQ0FBMEIsV0FBMUIsRUFBdUMsT0FBdkMsQ0FBK0M7QUFBQSxXQUFNLG1CQUFTLEVBQVQsQ0FBTjtBQUFBLEdBQS9DO0FBQ0EsV0FBUyxnQkFBVCxDQUEwQixlQUExQixFQUEyQyxPQUEzQyxDQUFtRDtBQUFBLFdBQU0sdUJBQWEsRUFBYixDQUFOO0FBQUEsR0FBbkQ7QUFDQTtBQUNBLFdBQVMsZ0JBQVQsQ0FBMEIsVUFBMUIsRUFBc0MsT0FBdEMsQ0FBOEM7QUFBQSxXQUFNLGtCQUFRLEVBQVIsQ0FBTjtBQUFBLEdBQTlDO0FBQ0EsV0FBUyxnQkFBVCxDQUEwQixhQUExQixFQUF5QyxPQUF6QyxDQUFpRDtBQUFBLFdBQU0scUJBQVcsRUFBWCxDQUFOO0FBQUEsR0FBakQ7QUFDQSxXQUFTLGdCQUFULENBQTBCLG9CQUExQixFQUFnRCxPQUFoRCxDQUF3RDtBQUFBLFdBQU0sNEJBQWtCLEVBQWxCLENBQU47QUFBQSxHQUF4RDtBQUNBLFdBQVMsZ0JBQVQsQ0FBMEIsbUJBQTFCLEVBQStDLE9BQS9DLENBQXVEO0FBQUEsV0FBTSwyQkFBaUIsRUFBakIsQ0FBTjtBQUFBLEdBQXZEO0FBQ0EsV0FBUyxnQkFBVCxDQUEwQix3QkFBMUIsRUFBb0QsT0FBcEQsQ0FBNEQ7QUFBQSxXQUFNLGdDQUFzQixFQUF0QixDQUFOO0FBQUEsR0FBNUQ7QUFDQSxXQUFTLGdCQUFULENBQTBCLGdCQUExQixFQUE0QyxPQUE1QyxDQUFvRDtBQUFBLFdBQU0sd0JBQWMsRUFBZCxDQUFOO0FBQUEsR0FBcEQ7QUFDRDs7QUFFRCxFQUFFLFlBQVk7QUFDWixJQUFFLGNBQUYsRUFBa0IsUUFBbEI7QUFDRCxDQUZEOztBQUlBLEVBQUUsWUFBWTtBQUNaLElBQUUsZ0JBQUYsRUFBb0IsUUFBcEI7QUFDRCxDQUZEOztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7SUNuRU0sSTtBQUNKLGdCQUFZLEdBQVosRUFBaUI7QUFBQTs7QUFDZixTQUFLLEdBQUwsR0FBVyxHQUFYO0FBQ0EsU0FBSyxNQUFMLEdBQWMsU0FBUyxhQUFULENBQXVCLEtBQUssR0FBTCxDQUFTLFlBQVQsQ0FBc0IsTUFBdEIsQ0FBdkIsQ0FBZDs7QUFFQSxTQUFLLFdBQUw7QUFDRDs7OztrQ0FFYTtBQUFBOztBQUNaLFdBQUssR0FBTCxDQUFTLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLGFBQUs7QUFDdEMsVUFBRSxjQUFGO0FBQ0EsY0FBSyxNQUFMLENBQVksYUFBWixDQUEwQixJQUFJLEtBQUosQ0FBVSxNQUFWLENBQTFCO0FBQ0QsT0FIRDtBQUlEOzs7Ozs7QUFHSCxPQUFPLE9BQVAsR0FBaUIsSUFBakI7Ozs7Ozs7OztJQ2hCTSxNO0FBQ0osa0JBQVksR0FBWixFQUFpQjtBQUFBOztBQUNmLFNBQUssR0FBTCxHQUFXLEdBQVg7O0FBRUEsU0FBSyxXQUFMO0FBQ0Q7Ozs7a0NBRWE7QUFDWixXQUFLLEdBQUwsQ0FBUyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxhQUFLO0FBQ3RDLFVBQUUsZUFBRjs7QUFFQSxpQkFBUyxhQUFULENBQXVCLElBQUksV0FBSixDQUFnQixhQUFoQixDQUF2QjtBQUNELE9BSkQ7QUFLRDs7Ozs7O0FBR0gsT0FBTyxPQUFQLEdBQWlCLE1BQWpCOzs7Ozs7Ozs7QUNoQkEsSUFBTSxlQUFlLFlBQXJCO0FBQ0EsSUFBTSxjQUFjLFlBQXBCOztJQUVNLEc7QUFDSixlQUFZLEdBQVosRUFBaUI7QUFBQTs7QUFDZixTQUFLLEdBQUwsR0FBVyxHQUFYOztBQUVBLFNBQUssV0FBTDtBQUNEOzs7O2tDQUVhO0FBQUE7O0FBQ1osV0FBSyxHQUFMLENBQVMsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsYUFBSztBQUN0QyxVQUFFLGVBQUY7QUFDQSxpQkFBUyxJQUFULENBQWMsU0FBZCxDQUF3QixHQUF4QixDQUE0QixXQUE1QjtBQUNBLGNBQUssR0FBTCxDQUFTLFNBQVQsQ0FBbUIsR0FBbkIsQ0FBdUIsWUFBdkI7O0FBRUEsaUJBQVMsYUFBVCxDQUF1QixJQUFJLFdBQUosQ0FBZ0IsYUFBaEIsRUFBK0IsRUFBRSxRQUFRLEVBQUUsS0FBSyxNQUFLLEdBQVosRUFBVixFQUEvQixDQUF2QjtBQUNELE9BTkQ7O0FBUUEsZUFBUyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxhQUFLO0FBQ3RDLGNBQUssR0FBTCxDQUFTLFNBQVQsQ0FBbUIsTUFBbkIsQ0FBMEIsWUFBMUI7QUFDQSxpQkFBUyxJQUFULENBQWMsU0FBZCxDQUF3QixNQUF4QixDQUErQixXQUEvQjtBQUNELE9BSEQ7O0FBS0EsZUFBUyxnQkFBVCxDQUEwQixhQUExQixFQUF5QyxVQUFDLENBQUQsRUFBSSxJQUFKLEVBQWE7QUFDcEQsWUFBSSxDQUFDLEVBQUUsTUFBSCxJQUFhLEVBQUUsTUFBRixDQUFTLEdBQVQsS0FBaUIsTUFBSyxHQUF2QyxFQUE0QztBQUMxQyxnQkFBSyxHQUFMLENBQVMsU0FBVCxDQUFtQixNQUFuQixDQUEwQixZQUExQjtBQUNEOztBQUVELFlBQUksQ0FBQyxFQUFFLE1BQVAsRUFBZTtBQUNiLG1CQUFTLElBQVQsQ0FBYyxTQUFkLENBQXdCLE1BQXhCLENBQStCLFdBQS9CO0FBQ0Q7QUFDRixPQVJEO0FBU0Q7Ozs7OztBQUdILE9BQU8sT0FBUCxHQUFpQixHQUFqQjs7Ozs7Ozs7O0lDcENNLFM7QUFDSixxQkFBWSxHQUFaLEVBQWlCO0FBQUE7O0FBQ2YsU0FBSyxHQUFMLEdBQVcsR0FBWDtBQUNBLFNBQUssS0FBTCxHQUFhLElBQUksZ0JBQUosQ0FBcUIscUJBQXJCLENBQWI7O0FBRUEsU0FBSyxTQUFMLEdBQWlCLFNBQVMsS0FBMUI7O0FBRUEsUUFBSSxPQUFPLFFBQVAsQ0FBZ0IsSUFBcEIsRUFBMEI7QUFDeEIsV0FBSyxhQUFMOztBQUVBLFVBQUksT0FBTyxRQUFQLENBQWdCLElBQWhCLEtBQXlCLE9BQXpCLElBQW9DLE9BQU8sUUFBUCxDQUFnQixJQUFoQixLQUF5QixPQUFqRSxFQUEwRTtBQUN4RSxhQUFLLE9BQUwsQ0FBYSxPQUFPLFFBQVAsQ0FBZ0IsSUFBN0I7QUFDRDtBQUNGOztBQUVELFNBQUssV0FBTDtBQUNEOzs7O2tDQUVhO0FBQUE7O0FBQ1osZUFBUyxnQkFBVCxDQUEwQixpQkFBMUIsRUFBNkMsYUFBSztBQUNoRCxjQUFLLE9BQUwsQ0FBYSxFQUFFLE1BQWY7QUFDRCxPQUZEOztBQUlBLFdBQUssS0FBTCxDQUFXLE9BQVgsQ0FBbUIsaUJBQVM7QUFDMUIsY0FBTSxnQkFBTixDQUF1QixPQUF2QixFQUFnQyxhQUFLO0FBQ25DLFlBQUUsY0FBRjs7QUFFQSxnQkFBSyxPQUFMO0FBQ0QsU0FKRDtBQUtELE9BTkQ7QUFPRDs7OzRCQUVPLEksRUFBTTtBQUNaLFVBQUksSUFBSixFQUFVO0FBQ1IsZ0JBQVEsU0FBUixDQUFrQixJQUFsQixFQUF3QixJQUF4QixRQUFrQyxJQUFsQztBQUNBLGVBQU8sUUFBUCxHQUFrQixJQUFsQjtBQUNBLGFBQUssTUFBTDtBQUNBLGFBQUssR0FBTCxDQUFTLFNBQVQsQ0FBbUIsR0FBbkIsVUFBOEIsSUFBOUI7QUFDQSxhQUFLLFdBQUwsR0FBbUIsSUFBbkI7QUFDQSxhQUFLLGFBQUw7QUFDRCxPQVBELE1BT087QUFDTCxnQkFBUSxTQUFSLENBQWtCLElBQWxCLEVBQXdCLEtBQUssU0FBN0IsRUFBd0MsT0FBTyxRQUFQLENBQWdCLFFBQXhEO0FBQ0EsZUFBTyxPQUFPLFFBQWQ7QUFDQSxhQUFLLE1BQUw7QUFDQSxhQUFLLGFBQUw7QUFDRDtBQUNGOzs7b0NBRWU7QUFDZCxXQUFLLEdBQUwsQ0FBUyxTQUFULENBQW1CLEdBQW5CLENBQXVCLG9CQUF2QjtBQUNEOzs7b0NBRWU7QUFDZCxXQUFLLEdBQUwsQ0FBUyxTQUFULENBQW1CLE1BQW5CLENBQTBCLG9CQUExQjtBQUNEOzs7NkJBRVE7QUFDUCxXQUFLLEdBQUwsQ0FBUyxTQUFULENBQW1CLE1BQW5CLFVBQWlDLEtBQUssV0FBdEM7QUFDRDs7Ozs7O0FBR0gsT0FBTyxPQUFQLEdBQWlCLFNBQWpCOzs7Ozs7Ozs7SUM3RE0saUI7QUFDSiw2QkFBWSxHQUFaLEVBQWlCO0FBQUE7O0FBQ2YsU0FBSyxHQUFMLEdBQVcsR0FBWDtBQUNBLFNBQUssTUFBTCxHQUFjLEtBQUssR0FBTCxDQUFTLFlBQVQsQ0FBc0IsTUFBdEIsRUFBOEIsS0FBOUIsQ0FBb0MsQ0FBcEMsQ0FBZDs7QUFFQSxTQUFLLFdBQUw7QUFDRDs7OztrQ0FFYTtBQUFBOztBQUNaLFdBQUssR0FBTCxDQUFTLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLGFBQUs7QUFDdEMsVUFBRSxjQUFGO0FBQ0EsaUJBQVMsYUFBVCxDQUF1QixJQUFJLFdBQUosQ0FBZ0IsaUJBQWhCLEVBQW1DLEVBQUUsVUFBVSxNQUFLLE1BQWpCLEVBQW5DLENBQXZCO0FBQ0QsT0FIRDtBQUlEOzs7Ozs7QUFHSCxPQUFPLE9BQVAsR0FBaUIsaUJBQWpCOzs7Ozs7Ozs7QUNoQkEsSUFBTSxlQUFlLFlBQXJCO0FBQ0EsSUFBTSxjQUFjLFdBQXBCOztJQUVNLE87QUFDSixtQkFBWSxHQUFaLEVBQWlCO0FBQUE7O0FBQ2YsU0FBSyxHQUFMLEdBQVcsR0FBWDtBQUNBLFNBQUssS0FBTCxHQUFhLFNBQVMsSUFBdEI7QUFDQSxTQUFLLE9BQUwsR0FBZSxNQUFmO0FBQ0EsU0FBSyxRQUFMLEdBQWdCLEtBQUssR0FBTCxDQUFTLFFBQVQsQ0FBa0IsQ0FBbEIsQ0FBaEI7QUFDQSxTQUFLLE1BQUwsR0FBYyxLQUFLLFFBQUwsQ0FBYyxhQUFkLENBQTRCLE9BQTVCLENBQWQ7QUFDQSxTQUFLLE1BQUwsR0FBYyxLQUFLLFFBQUwsQ0FBYyxhQUFkLENBQTRCLG9CQUE1QixDQUFkOztBQUVBLFNBQUssU0FBTCxHQUFpQixTQUFTLEtBQTFCO0FBQ0EsU0FBSyxLQUFMLEdBQWEsS0FBSyxHQUFMLENBQVMsYUFBVCxDQUF1QixJQUF2QixFQUE2QixTQUExQzs7QUFFQSxTQUFLLEVBQUwsR0FBVSxLQUFLLEdBQUwsQ0FBUyxFQUFuQjs7QUFFQSxRQUFJLE9BQU8sUUFBUCxDQUFnQixJQUFoQixXQUE2QixLQUFLLEVBQXRDLEVBQTRDO0FBQzFDLFdBQUssS0FBTDtBQUNEOztBQUVELFNBQUssV0FBTDtBQUNEOzs7O2tDQUVhO0FBQUE7O0FBQ1osV0FBSyxHQUFMLENBQVMsZ0JBQVQsQ0FBMEIsTUFBMUIsRUFBa0MsS0FBSyxLQUFMLENBQVcsSUFBWCxDQUFnQixJQUFoQixDQUFsQztBQUNBLFdBQUssR0FBTCxDQUFTLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLEtBQUssTUFBTCxDQUFZLElBQVosQ0FBaUIsSUFBakIsQ0FBbkM7QUFDQSxXQUFLLEdBQUwsQ0FBUyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxLQUFLLE1BQUwsQ0FBWSxJQUFaLENBQWlCLElBQWpCLENBQW5DO0FBQ0EsV0FBSyxNQUFMLENBQVksZ0JBQVosQ0FBNkIsT0FBN0IsRUFBc0MsS0FBSyxNQUFMLENBQVksSUFBWixDQUFpQixJQUFqQixDQUF0QztBQUNBLFdBQUssUUFBTCxDQUFjLFVBQWQsQ0FBeUIsT0FBekIsQ0FBaUMsaUJBQVM7QUFDeEMsY0FBTSxnQkFBTixDQUF1QixPQUF2QixFQUFnQztBQUFBLGlCQUFLLEVBQUUsZUFBRixFQUFMO0FBQUEsU0FBaEM7QUFDRCxPQUZEO0FBR0EsZUFBUyxnQkFBVCxDQUEwQixTQUExQixFQUFxQyxhQUFLO0FBQ3hDLFlBQUksRUFBRSxPQUFGLEtBQWMsRUFBbEIsRUFBc0IsTUFBSyxNQUFMO0FBQ3ZCLE9BRkQ7QUFHRDs7OzRCQUVPO0FBQ04sY0FBUSxHQUFSLENBQVksTUFBWjtBQUNBLFdBQUssR0FBTCxDQUFTLFNBQVQsQ0FBbUIsR0FBbkIsQ0FBdUIsWUFBdkI7QUFDQSxXQUFLLEtBQUwsQ0FBVyxTQUFYLENBQXFCLEdBQXJCLENBQXlCLFdBQXpCO0FBQ0EsZUFBUyxLQUFULEdBQWlCLEtBQUssS0FBdEI7QUFDQSxXQUFLLFlBQUwsQ0FBa0IsSUFBbEIsQ0FBdUIsSUFBdkI7O0FBRUEsVUFBSSxLQUFLLE1BQUwsSUFBZSxDQUFDLE9BQU8sUUFBM0IsRUFBcUM7QUFDbkMsYUFBSyxNQUFMLENBQVksSUFBWjtBQUNEOztBQUVELGNBQVEsU0FBUixDQUFrQixJQUFsQixFQUF3QixLQUFLLEtBQTdCLFFBQXdDLEtBQUssRUFBN0M7QUFFRDs7OzZCQUVRO0FBQ1AsY0FBUSxHQUFSLENBQVksT0FBWjtBQUNBLFdBQUssR0FBTCxDQUFTLFNBQVQsQ0FBbUIsTUFBbkIsQ0FBMEIsWUFBMUI7QUFDQSxXQUFLLEtBQUwsQ0FBVyxTQUFYLENBQXFCLE1BQXJCLENBQTRCLFdBQTVCO0FBQ0EsZUFBUyxLQUFULEdBQWlCLEtBQUssU0FBdEI7QUFDQSxjQUFRLFNBQVIsQ0FBa0IsSUFBbEIsRUFBd0IsS0FBSyxTQUE3QixFQUF3QyxPQUFPLFFBQVAsQ0FBZ0IsUUFBeEQ7QUFDQSxXQUFLLFlBQUwsQ0FBa0IsSUFBbEIsQ0FBdUIsSUFBdkI7O0FBRUEsVUFBSSxLQUFLLE1BQUwsSUFBZSxDQUFDLE9BQU8sUUFBM0IsRUFBcUM7QUFDbkMsYUFBSyxNQUFMLENBQVksS0FBWjtBQUNEOztBQUdELGVBQVMsYUFBVCxDQUF1QixJQUFJLFdBQUosQ0FBZ0IsaUJBQWhCLEVBQW1DLEVBQUUsVUFBVSxPQUFPLFFBQW5CLEVBQW5DLENBQXZCO0FBQ0Q7OzttQ0FFYztBQUNiLGNBQVEsR0FBUixDQUFZLGNBQVosRUFBNEIsSUFBNUI7QUFDQSxVQUFJLEtBQUssT0FBTCxDQUFhLGdCQUFqQixFQUFtQztBQUNqQyxZQUFJLGFBQWEsUUFBakIsRUFBMkI7QUFDekI7QUFDQSxlQUFLLE9BQUwsQ0FBYSxnQkFBYixDQUE4QixPQUE5QixFQUF1QyxLQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLElBQWxCLENBQXZDO0FBQ0QsU0FIRCxNQUdPLElBQUksa0JBQWtCLFFBQXRCLEVBQWdDO0FBQ3JDO0FBQ0EsZUFBSyxPQUFMLENBQWEsZ0JBQWIsQ0FBOEIsWUFBOUIsRUFBNEMsS0FBSyxPQUFMLENBQWEsSUFBYixDQUFrQixJQUFsQixDQUE1QztBQUNELFNBSE0sTUFHQTtBQUNMO0FBQ0EsZUFBSyxPQUFMLENBQWEsZ0JBQWIsQ0FBOEIscUJBQTlCLEVBQXFELEtBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsSUFBbEIsQ0FBckQ7QUFDRDtBQUNGLE9BWEQsTUFXTztBQUFFO0FBQ1AsYUFBSyxPQUFMLENBQWEsV0FBYixDQUF5QixjQUF6QixFQUF5QyxLQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLElBQWxCLENBQXpDO0FBQ0Q7QUFDRjs7O21DQUVjO0FBQ2IsY0FBUSxHQUFSLENBQVksY0FBWixFQUE0QixJQUE1QjtBQUNBLFVBQUksS0FBSyxPQUFMLENBQWEsbUJBQWpCLEVBQXNDO0FBQ3BDLFlBQUksYUFBYSxRQUFqQixFQUEyQjtBQUN6QjtBQUNBLGVBQUssT0FBTCxDQUFhLG1CQUFiLENBQWlDLE9BQWpDLEVBQTBDLEtBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsSUFBbEIsQ0FBMUM7QUFDRCxTQUhELE1BR08sSUFBSSxrQkFBa0IsUUFBdEIsRUFBZ0M7QUFDckM7QUFDQSxlQUFLLE9BQUwsQ0FBYSxtQkFBYixDQUFpQyxZQUFqQyxFQUErQyxLQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLElBQWxCLENBQS9DO0FBQ0QsU0FITSxNQUdBO0FBQ0w7QUFDQSxlQUFLLE9BQUwsQ0FBYSxtQkFBYixDQUFpQyxxQkFBakMsRUFBd0QsS0FBSyxPQUFMLENBQWEsSUFBYixDQUFrQixJQUFsQixDQUF4RDtBQUNEO0FBQ0YsT0FYRCxNQVdPO0FBQUU7QUFDUCxhQUFLLE9BQUwsQ0FBYSxXQUFiLENBQXlCLGNBQXpCLEVBQXlDLEtBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsSUFBbEIsQ0FBekM7QUFDRDtBQUNGOzs7NEJBRU8sQyxFQUFHO0FBQ1QsY0FBUSxHQUFSLENBQVksUUFBWjtBQUNBLGVBQVMsTUFBVCxDQUFnQixLQUFoQixFQUF1QixNQUF2QixFQUErQjtBQUM3QixZQUFJLE1BQU0sT0FBTyxTQUFQLEtBQXFCLEtBQS9CO0FBQ0EsZUFBTyxTQUFQLENBQWlCLEdBQWpCO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLLE9BQU8sS0FBaEI7O0FBRUEsVUFBSSxRQUFRLEVBQUUsTUFBRixJQUFZLEVBQUUsTUFBZCxJQUF3QixFQUFFLFVBQXRDOztBQUVBLGFBQU8sS0FBUCxFQUFjLEtBQUssR0FBbkI7O0FBRUEsUUFBRSxjQUFGLEdBQW1CLEVBQUUsY0FBRixFQUFuQixHQUF5QyxFQUFFLFdBQUYsR0FBZ0IsS0FBekQ7QUFDRDs7Ozs7O0FBRUgsT0FBTyxPQUFQLEdBQWlCLE9BQWpCOzs7Ozs7Ozs7SUN4SE0sWTtBQUNKLHdCQUFZLEdBQVosRUFBaUI7QUFBQTs7QUFDZixTQUFLLEdBQUwsR0FBVyxHQUFYOztBQUVBLFNBQUssU0FBTCxHQUFpQixJQUFqQjs7QUFFQSxRQUFJLE9BQU8sUUFBWCxFQUFxQjtBQUNuQixXQUFLLEdBQUwsQ0FBUyxZQUFULENBQXNCLFVBQXRCLEVBQWtDLFVBQWxDO0FBQ0Q7O0FBRUQsU0FBSyxXQUFMO0FBQ0Q7Ozs7a0NBRWE7QUFDWixXQUFLLEdBQUwsQ0FBUyxnQkFBVCxDQUEwQixXQUExQixFQUF1QyxLQUFLLGFBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBdkM7QUFDQSxXQUFLLEdBQUwsQ0FBUyxnQkFBVCxDQUEwQixVQUExQixFQUFzQyxLQUFLLGFBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBdEM7QUFDQSxXQUFLLEdBQUwsQ0FBUyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxLQUFLLFVBQUwsQ0FBZ0IsSUFBaEIsQ0FBcUIsSUFBckIsQ0FBbkM7QUFDRDs7O2lDQUVZO0FBQ1gsV0FBSyxTQUFMLEdBQWlCLEtBQUssR0FBTCxDQUFTLEtBQVQsRUFBakIsR0FBb0MsS0FBSyxHQUFMLENBQVMsSUFBVCxFQUFwQztBQUNBLFdBQUssU0FBTCxHQUFpQixDQUFDLEtBQUssU0FBdkI7QUFDRDs7O29DQUVlO0FBQ2QsV0FBSyxHQUFMLENBQVMsUUFBVCxHQUFvQixJQUFwQjtBQUNEOzs7b0NBRWU7QUFDZCxXQUFLLEdBQUwsQ0FBUyxRQUFULEdBQW9CLEtBQXBCO0FBQ0Q7Ozs7OztBQUdILE9BQU8sT0FBUCxHQUFpQixZQUFqQjs7Ozs7Ozs7O0FDakNBLElBQU0sZ0JBQWdCLGFBQXRCO0FBQ0EsSUFBTSxjQUFjLFlBQXBCO0FBQ0EsSUFBTSxnQkFBZ0IsQ0FBdEI7O0lBRU0sSTtBQUNKLGdCQUFZLEdBQVosRUFBaUI7QUFBQTs7QUFDZixTQUFLLEdBQUwsR0FBVyxHQUFYOztBQUVBLFNBQUssWUFBTDtBQUNEOzs7O21DQUVjO0FBQ2IsVUFBTSxPQUFPLEtBQUssR0FBTCxDQUFTLFNBQXRCO0FBQ0EsV0FBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLGFBQXBCLEVBQW1DLEdBQW5DLEVBQXdDO0FBQ3RDLFlBQU0sUUFBUSxLQUFLLEdBQUwsQ0FBUyxTQUFULEVBQWQ7QUFDQSxjQUFNLGVBQU4sQ0FBc0IsSUFBdEI7QUFDQSxjQUFNLFNBQU4sQ0FBZ0IsTUFBaEIsQ0FBdUIsTUFBdkI7QUFDQSxjQUFNLFNBQU4sQ0FBZ0IsTUFBaEIsQ0FBdUIsVUFBdkI7QUFDQSxjQUFNLFNBQU4sQ0FBZ0IsR0FBaEIsQ0FBb0IsV0FBcEI7QUFDQSxjQUFNLFNBQU4sR0FBa0IsSUFBbEI7QUFDQSxhQUFLLEdBQUwsQ0FBUyxXQUFULENBQXFCLEtBQXJCO0FBQ0Q7QUFDRjs7Ozs7O0FBR0gsT0FBTyxPQUFQLEdBQWlCLElBQWpCOzs7Ozs7Ozs7SUN6Qk0sUTtBQUNKLG9CQUFZLEdBQVosRUFBaUI7QUFBQTs7QUFDZixTQUFLLEdBQUwsR0FBVyxHQUFYOztBQUVBLFNBQUssV0FBTDtBQUNEOzs7O2tDQUVhO0FBQUE7O0FBQ1osV0FBSyxHQUFMLENBQVMsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsYUFBSztBQUN0QyxjQUFLLEdBQUwsQ0FBUyxTQUFULElBQXNCLEVBQUUsV0FBeEI7QUFDQSxVQUFFLGNBQUY7QUFDRCxPQUhEO0FBSUQ7Ozs7OztBQUdILE9BQU8sT0FBUCxHQUFpQixRQUFqQjs7Ozs7Ozs7O0FDZkEsSUFBTSxlQUFlLE9BQU8sUUFBUCxHQUFrQixtQkFBbEIsR0FBd0MsT0FBN0Q7QUFDQSxJQUFNLGdCQUFnQixDQUF0Qjs7SUFFTSxhO0FBQ0oseUJBQVksR0FBWixFQUFpQjtBQUFBOztBQUNmLFNBQUssR0FBTCxHQUFXLEdBQVg7QUFDQSxTQUFLLEdBQUwsR0FBVyxLQUFLLEdBQUwsQ0FBUyxpQkFBcEI7QUFDQSxTQUFLLEtBQUwsR0FBYSxLQUFLLEdBQUwsQ0FBUyxpQkFBdEI7O0FBRUEsU0FBSyxNQUFMLEdBQWMsRUFBZDtBQUNBLFNBQUssUUFBTCxHQUFnQixLQUFLLEdBQUwsQ0FBUyxPQUFULENBQWlCLFFBQWpCLEdBQTRCLGFBQTVDO0FBQ0EsU0FBSyxVQUFMLEdBQWtCLEtBQUssR0FBTCxDQUFTLE9BQVQsQ0FBaUIsT0FBbkM7O0FBRUEsU0FBSyxjQUFMO0FBQ0EsU0FBSyxXQUFMO0FBQ0EsU0FBSyxLQUFMO0FBQ0Q7Ozs7cUNBRWdCO0FBQ2YsV0FBSyxNQUFMLEdBQWMsU0FBUyxhQUFULENBQXVCLE9BQXZCLENBQWQ7QUFDQSxlQUFTLElBQVQsQ0FBYyxXQUFkLENBQTBCLEtBQUssTUFBL0I7QUFDRDs7OzRCQUVPO0FBQ04sVUFBTSxjQUFjLE9BQU8sVUFBM0I7QUFDQSxXQUFLLEtBQUwsR0FBYSxLQUFLLEdBQUwsQ0FBUyxXQUF0Qjs7QUFFQSxVQUFJLEtBQUssS0FBVCxFQUFnQjtBQUNkLFlBQU0sZUFBZSxLQUFLLEtBQUwsQ0FBVyxjQUFjLEtBQUssS0FBOUIsSUFBdUMsQ0FBNUQ7QUFDQSxhQUFLLFVBQUwsR0FBa0IsZUFBZSxLQUFLLEtBQXBCLEdBQTRCLFdBQTlDOztBQUVBLGFBQUssSUFBSSxJQUFJLENBQWIsRUFBZ0IsSUFBSSxZQUFwQixFQUFrQyxHQUFsQyxFQUF1QztBQUNyQyxjQUFNLFNBQVMsS0FBSyxLQUFMLENBQVcsU0FBWCxDQUFxQixJQUFyQixDQUFmOztBQUVBLGVBQUssTUFBTCxDQUFZLElBQVosQ0FBaUIsTUFBakI7QUFDQSxlQUFLLEdBQUwsQ0FBUyxXQUFULENBQXFCLE1BQXJCO0FBQ0Q7O0FBRUQsYUFBSyxNQUFMLEdBQWMsS0FBSyxHQUFMLENBQVMsV0FBVCxHQUF1QixhQUFyQzs7QUFFQSxhQUFLLElBQUksS0FBSSxDQUFiLEVBQWdCLEtBQUksYUFBcEIsRUFBbUMsSUFBbkMsRUFBd0M7QUFDdEMsY0FBTSxVQUFTLEtBQUssR0FBTCxDQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBZjtBQUNBLGVBQUssTUFBTCxDQUFZLElBQVosQ0FBaUIsT0FBakI7QUFDQSxlQUFLLEdBQUwsQ0FBUyxXQUFULENBQXFCLE9BQXJCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFLLGNBQUw7QUFDRDs7O3FDQUVnQjtBQUNmLFVBQU0sWUFBVyxJQUFJLElBQUosRUFBRCxDQUFhLE9BQWIsRUFBaEI7O0FBRUEsV0FBSyxHQUFMLENBQVMsRUFBVCxHQUFjLEVBQWQ7O0FBRUEsVUFBTSxPQUFPLEtBQUssVUFBTCxHQUFrQixDQUFDLEtBQUssTUFBeEIsR0FBaUMsQ0FBOUM7QUFDQSxVQUFNLE9BQU8sS0FBSyxVQUFMLEdBQWtCLENBQWxCLEdBQXNCLENBQUMsS0FBSyxNQUF6Qzs7QUFFQSxVQUFNLHVDQUNnQixFQURoQiw4Q0FFMEIsSUFGMUIsa0VBRzRCLElBSDVCLGdEQU1ELEVBTkMsc0NBT2tCLEVBUGxCLFNBT3dCLEtBQUssUUFQN0Isc0NBQU47O0FBV0EsV0FBSyxNQUFMLENBQVksV0FBWixDQUF3QixTQUFTLGNBQVQsQ0FBd0IsS0FBeEIsQ0FBeEI7QUFDRDs7OzhCQUVTO0FBQ1IsV0FBSyxhQUFMO0FBQ0EsV0FBSyxLQUFMO0FBQ0Q7OztvQ0FFZTtBQUNkLFdBQUssTUFBTCxDQUFZLE9BQVosQ0FBb0I7QUFBQSxlQUFVLE9BQU8sTUFBUCxFQUFWO0FBQUEsT0FBcEI7QUFDQSxXQUFLLE1BQUwsR0FBYyxFQUFkO0FBQ0Q7OztrQ0FFYTtBQUNaLGFBQU8sZ0JBQVAsQ0FBd0IsWUFBeEIsRUFBc0MsS0FBSyxPQUFMLENBQWEsSUFBYixDQUFrQixJQUFsQixDQUF0QztBQUNEOzs7Ozs7QUFHSCxPQUFPLE9BQVAsR0FBaUIsYUFBakI7Ozs7Ozs7OztBQ3ZGQSxJQUFNLGdCQUFnQixhQUF0Qjs7SUFFTSxLO0FBQ0osaUJBQVksR0FBWixFQUFpQjtBQUFBOztBQUNmLFNBQUssR0FBTCxHQUFXLEdBQVg7O0FBRUEsU0FBSyxXQUFMO0FBQ0Q7Ozs7a0NBRWE7QUFDWixXQUFLLEdBQUwsQ0FBUyxnQkFBVCxDQUEwQixXQUExQixFQUF1QyxLQUFLLEtBQUwsQ0FBVyxJQUFYLENBQWdCLElBQWhCLENBQXZDO0FBQ0EsV0FBSyxHQUFMLENBQVMsZ0JBQVQsQ0FBMEIsVUFBMUIsRUFBc0MsS0FBSyxNQUFMLENBQVksSUFBWixDQUFpQixJQUFqQixDQUF0QztBQUNBLFdBQUssR0FBTCxDQUFTLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DO0FBQUEsZUFBSyxFQUFFLGNBQUYsRUFBTDtBQUFBLE9BQW5DO0FBQ0Q7Ozs0QkFFTztBQUNOLFdBQUssR0FBTCxDQUFTLElBQVQ7QUFDQSxXQUFLLEdBQUwsQ0FBUyxTQUFULENBQW1CLEdBQW5CLENBQXVCLGFBQXZCO0FBQ0Q7Ozs2QkFFUTtBQUNQLFdBQUssR0FBTCxDQUFTLEtBQVQ7QUFDQSxXQUFLLEdBQUwsQ0FBUyxTQUFULENBQW1CLE1BQW5CLENBQTBCLGFBQTFCO0FBQ0Q7Ozs7OztBQUdILE9BQU8sT0FBUCxHQUFpQixLQUFqQiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJpZigvQW5kcm9pZHx3ZWJPU3xpUGhvbmV8aVBhZHxpUG9kfEJsYWNrQmVycnl8SUVNb2JpbGV8T3BlcmEgTWluaS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCkpIHtcbiAgd2luZG93LmlzTW9iaWxlID0gdHJ1ZVxuICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaXNfX21vYmlsZScpXG5cbiAgY29uc3Qgc2V0T3JpZW50YXRpb24gPSAoKSA9PiB7XG4gICAgaWYgKHdpbmRvdy5pbm5lckhlaWdodCA8IHdpbmRvdy5pbm5lcldpZHRoKSB7XG4gICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaXNfX2hvcml6b250YWwnKVxuICAgIH0gZWxzZSB7XG4gICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnaXNfX2hvcml6b250YWwnKVxuICAgIH1cbiAgfVxuXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdvcmllbnRhdGlvbmNoYW5nZScsIHNldE9yaWVudGF0aW9uKVxuICBzZXRPcmllbnRhdGlvbigpXG59XG5cbmRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QuYWRkKCdpc19fcmVhZHknKVxuXG5pZiAobG9jYXRpb24ucGF0aG5hbWUuc3BsaXQoJy8nKS5maWx0ZXIoQm9vbGVhbikucG9wKCkgPT09ICdlbicpIHtcbiAgY29uc3QgbGlua3MgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5wb3J0Zm9saW9MaW5rLWxpbmssIC5sb2dvLWRvdC1saW5rJykpXG4gIGxpbmtzLmZvckVhY2gobGluayA9PiBsaW5rLmhyZWYgJiYgKGxpbmsuaHJlZiA9ICdlbi8nICsgbGluay5ocmVmKSlcbn1cblxuXG5cbmltcG9ydCBWaWRlbyBmcm9tICcuL2FwcC92aWRlbydcbmltcG9ydCBQcm9qZWN0IGZyb20gJy4vYXBwL3Byb2plY3QnXG5pbXBvcnQgQ2FyZCBmcm9tICcuL2FwcC9jYXJkJ1xuaW1wb3J0IFNjYW4gZnJvbSAnLi9hcHAvc2NhbidcbmltcG9ydCBTY3JvbGxlciBmcm9tICcuL2FwcC9zY3JvbGxlcidcbi8vaW1wb3J0IEZvdG9yYW1hIGZyb20gJy4vYXBwL2ZvdG9yYW1hJ1xuaW1wb3J0IERvdCBmcm9tICcuL2FwcC9kb3QnXG5pbXBvcnQgQ2xvc2VyIGZyb20gJy4vYXBwL2Nsb3NlcidcbmltcG9ydCBTY3JvbGxpbmdUZXh0IGZyb20gJy4vYXBwL3Njcm9sbGluZ1RleHQnXG5pbXBvcnQgUHJvamVjdFZpZGVvIGZyb20gJy4vYXBwL3Byb2plY3RWaWRlbydcbmltcG9ydCBQb3J0Zm9saW9TZWxlY3RvciBmcm9tICcuL2FwcC9wb3J0Zm9saW9TZWxlY3RvcidcbmltcG9ydCBQb3J0Zm9saW8gZnJvbSAnLi9hcHAvcG9ydGZvbGlvJ1xuXG57XG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qc19fdmlkZW8nKS5mb3JFYWNoKGVsID0+IG5ldyBWaWRlbyhlbCkpXG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qc19fcHJvamVjdCcpLmZvckVhY2goZWwgPT4gbmV3IFByb2plY3QoZWwpKVxuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuanNfX2NhcmQnKS5mb3JFYWNoKGVsID0+IG5ldyBDYXJkKGVsKSlcbiAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzX19zY2FuJykuZm9yRWFjaChlbCA9PiBuZXcgU2NhbihlbCkpXG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qc19fc2Nyb2xsZXInKS5mb3JFYWNoKGVsID0+IG5ldyBTY3JvbGxlcihlbCkpXG4gIC8vZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzX3NsaWNrc2xpZGVyJykuZm9yRWFjaChlbCA9PiBuZXcgU2xpY2tTbGlkZXIoZWwpKVxuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuanNfX2RvdCcpLmZvckVhY2goZWwgPT4gbmV3IERvdChlbCkpXG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qc19fY2xvc2VyJykuZm9yRWFjaChlbCA9PiBuZXcgQ2xvc2VyKGVsKSlcbiAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzX19zY3JvbGxpbmdUZXh0JykuZm9yRWFjaChlbCA9PiBuZXcgU2Nyb2xsaW5nVGV4dChlbCkpXG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qc19fcHJvamVjdFZpZGVvJykuZm9yRWFjaChlbCA9PiBuZXcgUHJvamVjdFZpZGVvKGVsKSlcbiAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzX19wb3J0Zm9saW9TZWxlY3RvcicpLmZvckVhY2goZWwgPT4gbmV3IFBvcnRmb2xpb1NlbGVjdG9yKGVsKSlcbiAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzX19wb3J0Zm9saW8nKS5mb3JFYWNoKGVsID0+IG5ldyBQb3J0Zm9saW8oZWwpKVxufVxuXG4kKGZ1bmN0aW9uICgpIHtcbiAgJCgnLnNtYXJ0LXNoYWZ0JykuZm90b3JhbWEoKTtcbn0pO1xuXG4kKGZ1bmN0aW9uICgpIHtcbiAgJCgnLnRlYW0tY2Fyb3VzZWwnKS5mb3RvcmFtYSgpO1xufSk7XG5cbi8vICQoZnVuY3Rpb24gKCkge1xuLy8gICAkKCcudGVhbS1jYXJvdXNlbCcpLm93bENhcm91c2VsKHtcbi8vICAgICBsb29wOnRydWUsXG4vLyAgICAgbWFyZ2luOjEwLFxuLy8gICAgIG5hdjp0cnVlLFxuLy8gfSk7XG4vLyB9KTtcblxuIiwiY2xhc3MgQ2FyZCB7XG4gIGNvbnN0cnVjdG9yKCRlbCkge1xuICAgIHRoaXMuJGVsID0gJGVsXG4gICAgdGhpcy50YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRoaXMuJGVsLmdldEF0dHJpYnV0ZSgnaHJlZicpKVxuXG4gICAgdGhpcy5fYmluZEV2ZW50cygpXG4gIH1cblxuICBfYmluZEV2ZW50cygpIHtcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgdGhpcy50YXJnZXQuZGlzcGF0Y2hFdmVudChuZXcgRXZlbnQoJ29wZW4nKSlcbiAgICB9KVxuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gQ2FyZFxuIiwiY2xhc3MgQ2xvc2VyIHtcbiAgY29uc3RydWN0b3IoJGVsKSB7XG4gICAgdGhpcy4kZWwgPSAkZWxcblxuICAgIHRoaXMuX2JpbmRFdmVudHMoKVxuICB9XG5cbiAgX2JpbmRFdmVudHMoKSB7XG4gICAgdGhpcy4kZWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBlID0+IHtcbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKClcblxuICAgICAgZG9jdW1lbnQuZGlzcGF0Y2hFdmVudChuZXcgQ3VzdG9tRXZlbnQoJ2Nsb3NlUG9wdXBzJykpXG4gICAgfSlcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IENsb3NlclxuIiwiY29uc3QgQ0xBU1NfQUNUSVZFID0gJ2lzX19hY3RpdmUnXG5jb25zdCBDTEFTU19QT1BVUCA9ICdoYXNfX3BvcHVwJ1xuXG5jbGFzcyBEb3Qge1xuICBjb25zdHJ1Y3RvcigkZWwpIHtcbiAgICB0aGlzLiRlbCA9ICRlbFxuXG4gICAgdGhpcy5fYmluZEV2ZW50cygpXG4gIH1cblxuICBfYmluZEV2ZW50cygpIHtcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuYWRkKENMQVNTX1BPUFVQKVxuICAgICAgdGhpcy4kZWwuY2xhc3NMaXN0LmFkZChDTEFTU19BQ1RJVkUpXG5cbiAgICAgIGRvY3VtZW50LmRpc3BhdGNoRXZlbnQobmV3IEN1c3RvbUV2ZW50KCdjbG9zZVBvcHVwcycsIHsgZGV0YWlsOiB7ICRlbDogdGhpcy4kZWwgfX0pKVxuICAgIH0pXG5cbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIF8gPT4ge1xuICAgICAgdGhpcy4kZWwuY2xhc3NMaXN0LnJlbW92ZShDTEFTU19BQ1RJVkUpXG4gICAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5yZW1vdmUoQ0xBU1NfUE9QVVApXG4gICAgfSlcblxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2Nsb3NlUG9wdXBzJywgKGUsIGRhdGEpID0+IHtcbiAgICAgIGlmICghZS5kZXRhaWwgfHwgZS5kZXRhaWwuJGVsICE9PSB0aGlzLiRlbCkge1xuICAgICAgICB0aGlzLiRlbC5jbGFzc0xpc3QucmVtb3ZlKENMQVNTX0FDVElWRSlcbiAgICAgIH1cblxuICAgICAgaWYgKCFlLmRldGFpbCkge1xuICAgICAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5yZW1vdmUoQ0xBU1NfUE9QVVApXG4gICAgICB9XG4gICAgfSlcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IERvdFxuIiwiY2xhc3MgUG9ydGZvbGlvIHtcbiAgY29uc3RydWN0b3IoJGVsKSB7XG4gICAgdGhpcy4kZWwgPSAkZWxcbiAgICB0aGlzLiRiYWNrID0gJGVsLnF1ZXJ5U2VsZWN0b3JBbGwoJy5qc19fcG9ydGZvbGlvLWJhY2snKVxuXG4gICAgdGhpcy5iYXNlVGl0bGUgPSBkb2N1bWVudC50aXRsZVxuXG4gICAgaWYgKHdpbmRvdy5sb2NhdGlvbi5oYXNoKSB7XG4gICAgICB0aGlzLl9oaWRlU2VsZWN0b3IoKVxuXG4gICAgICBpZiAod2luZG93LmxvY2F0aW9uLmhhc2ggPT09ICdwaG90bycgfHwgd2luZG93LmxvY2F0aW9uLmhhc2ggPT09ICd2aWRlbycpIHtcbiAgICAgICAgdGhpcy5fZmlsdGVyKHdpbmRvdy5sb2NhdGlvbi5oYXNoKVxuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuX2JpbmRFdmVudHMoKVxuICB9XG5cbiAgX2JpbmRFdmVudHMoKSB7XG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigncG9ydGZvbGlvRmlsdGVyJywgZSA9PiB7XG4gICAgICB0aGlzLl9maWx0ZXIoZS5kZXRhaWwpXG4gICAgfSlcblxuICAgIHRoaXMuJGJhY2suZm9yRWFjaCgkYmFjayA9PiB7XG4gICAgICAkYmFjay5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KClcblxuICAgICAgICB0aGlzLl9maWx0ZXIoKVxuICAgICAgfSlcbiAgICB9KVxuICB9XG5cbiAgX2ZpbHRlcih0eXBlKSB7XG4gICAgaWYgKHR5cGUpIHtcbiAgICAgIGhpc3RvcnkucHVzaFN0YXRlKG51bGwsIHR5cGUsIGAjJHt0eXBlfWApXG4gICAgICB3aW5kb3cucHJldlR5cGUgPSB0eXBlO1xuICAgICAgdGhpcy5fY2xlYXIoKVxuICAgICAgdGhpcy4kZWwuY2xhc3NMaXN0LmFkZChgaXNfXyR7dHlwZX1gKVxuICAgICAgdGhpcy5jdXJyZW50VHlwZSA9IHR5cGVcbiAgICAgIHRoaXMuX2hpZGVTZWxlY3RvcigpXG4gICAgfSBlbHNlIHtcbiAgICAgIGhpc3RvcnkucHVzaFN0YXRlKG51bGwsIHRoaXMuYmFzZVRpdGxlLCB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUpXG4gICAgICBkZWxldGUgd2luZG93LnByZXZUeXBlO1xuICAgICAgdGhpcy5fY2xlYXIoKVxuICAgICAgdGhpcy5fc2hvd1NlbGVjdG9yKClcbiAgICB9XG4gIH1cblxuICBfaGlkZVNlbGVjdG9yKCkge1xuICAgIHRoaXMuJGVsLmNsYXNzTGlzdC5hZGQoJ2lzX19oaWRkZW5TZWxlY3RvcicpXG4gIH1cblxuICBfc2hvd1NlbGVjdG9yKCkge1xuICAgIHRoaXMuJGVsLmNsYXNzTGlzdC5yZW1vdmUoJ2lzX19oaWRkZW5TZWxlY3RvcicpXG4gIH1cblxuICBfY2xlYXIoKSB7XG4gICAgdGhpcy4kZWwuY2xhc3NMaXN0LnJlbW92ZShgaXNfXyR7dGhpcy5jdXJyZW50VHlwZX1gKVxuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gUG9ydGZvbGlvXG4iLCJjbGFzcyBQb3J0Zm9saW9TZWxlY3RvciB7XG4gIGNvbnN0cnVjdG9yKCRlbCkge1xuICAgIHRoaXMuJGVsID0gJGVsXG4gICAgdGhpcy50YXJnZXQgPSB0aGlzLiRlbC5nZXRBdHRyaWJ1dGUoJ2hyZWYnKS5zbGljZSgxKVxuXG4gICAgdGhpcy5fYmluZEV2ZW50cygpXG4gIH1cblxuICBfYmluZEV2ZW50cygpIHtcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgZG9jdW1lbnQuZGlzcGF0Y2hFdmVudChuZXcgQ3VzdG9tRXZlbnQoJ3BvcnRmb2xpb0ZpbHRlcicsIHsgJ2RldGFpbCc6IHRoaXMudGFyZ2V0IH0pKVxuICAgIH0pXG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBQb3J0Zm9saW9TZWxlY3RvclxuIiwiY29uc3QgQ0xBU1NfT1BFTkVEID0gJ2lzX19vcGVuZWQnXG5jb25zdCBDTEFTU19GQURFRCA9ICdpc19fZmFkZWQnXG5cbmNsYXNzIFByb2plY3Qge1xuICBjb25zdHJ1Y3RvcigkZWwpIHtcbiAgICB0aGlzLiRlbCA9ICRlbFxuICAgIHRoaXMuJGJvZHkgPSBkb2N1bWVudC5ib2R5XG4gICAgdGhpcy4kd2luZG93ID0gd2luZG93IFxuICAgIHRoaXMuJGNvbnRlbnQgPSB0aGlzLiRlbC5jaGlsZHJlblswXVxuICAgIHRoaXMuJHZpZGVvID0gdGhpcy4kY29udGVudC5xdWVyeVNlbGVjdG9yKCd2aWRlbycpXG4gICAgdGhpcy4kY2xvc2UgPSB0aGlzLiRjb250ZW50LnF1ZXJ5U2VsZWN0b3IoJy5qc19fcHJvamVjdC1jbG9zZScpXG5cbiAgICB0aGlzLmJhc2VUaXRsZSA9IGRvY3VtZW50LnRpdGxlXG4gICAgdGhpcy50aXRsZSA9IHRoaXMuJGVsLnF1ZXJ5U2VsZWN0b3IoJ2gyJykuaW5uZXJUZXh0XG5cbiAgICB0aGlzLmlkID0gdGhpcy4kZWwuaWRcblxuICAgIGlmICh3aW5kb3cubG9jYXRpb24uaGFzaCA9PT0gYCMke3RoaXMuaWR9YCkge1xuICAgICAgdGhpcy5fb3BlbigpXG4gICAgfVxuXG4gICAgdGhpcy5fYmluZEV2ZW50cygpXG4gIH1cblxuICBfYmluZEV2ZW50cygpIHtcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCdvcGVuJywgdGhpcy5fb3Blbi5iaW5kKHRoaXMpKVxuICAgIHRoaXMuJGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2Nsb3NlJywgdGhpcy5fY2xvc2UuYmluZCh0aGlzKSlcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuX2Nsb3NlLmJpbmQodGhpcykpXG4gICAgdGhpcy4kY2xvc2UuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLl9jbG9zZS5iaW5kKHRoaXMpKVxuICAgIHRoaXMuJGNvbnRlbnQuY2hpbGROb2Rlcy5mb3JFYWNoKGNoaWxkID0+IHtcbiAgICAgIGNoaWxkLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZSA9PiBlLnN0b3BQcm9wYWdhdGlvbigpKVxuICAgIH0pXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIGUgPT4ge1xuICAgICAgaWYgKGUua2V5Q29kZSA9PT0gMjcpIHRoaXMuX2Nsb3NlKClcbiAgICB9KVxuICB9XG5cbiAgX29wZW4oKSB7XG4gICAgY29uc29sZS5sb2coXCJvcGVuXCIpO1xuICAgIHRoaXMuJGVsLmNsYXNzTGlzdC5hZGQoQ0xBU1NfT1BFTkVEKVxuICAgIHRoaXMuJGJvZHkuY2xhc3NMaXN0LmFkZChDTEFTU19GQURFRClcbiAgICBkb2N1bWVudC50aXRsZSA9IHRoaXMudGl0bGVcbiAgICB0aGlzLl9hdHRhY2hXaGVlbC5iaW5kKHRoaXMpO1xuXG4gICAgaWYgKHRoaXMuJHZpZGVvICYmICF3aW5kb3cuaXNNb2JpbGUpIHtcbiAgICAgIHRoaXMuJHZpZGVvLnBsYXkoKVxuICAgIH1cblxuICAgIGhpc3RvcnkucHVzaFN0YXRlKG51bGwsIHRoaXMudGl0bGUsIGAjJHt0aGlzLmlkfWApO1xuICAgIFxuICB9XG5cbiAgX2Nsb3NlKCkge1xuICAgIGNvbnNvbGUubG9nKFwiY2xvc2VcIik7XG4gICAgdGhpcy4kZWwuY2xhc3NMaXN0LnJlbW92ZShDTEFTU19PUEVORUQpXG4gICAgdGhpcy4kYm9keS5jbGFzc0xpc3QucmVtb3ZlKENMQVNTX0ZBREVEKVxuICAgIGRvY3VtZW50LnRpdGxlID0gdGhpcy5iYXNlVGl0bGVcbiAgICBoaXN0b3J5LnB1c2hTdGF0ZShudWxsLCB0aGlzLmJhc2VUaXRsZSwgd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lKTtcbiAgICB0aGlzLl9yZW1vdmVXaGVlbC5iaW5kKHRoaXMpO1xuXG4gICAgaWYgKHRoaXMuJHZpZGVvICYmICF3aW5kb3cuaXNNb2JpbGUpIHtcbiAgICAgIHRoaXMuJHZpZGVvLnBhdXNlKClcbiAgICB9XG4gICAgXG5cbiAgICBkb2N1bWVudC5kaXNwYXRjaEV2ZW50KG5ldyBDdXN0b21FdmVudCgncG9ydGZvbGlvRmlsdGVyJywgeyAnZGV0YWlsJzogd2luZG93LnByZXZUeXBlIH0pKVxuICB9XG5cbiAgX2F0dGFjaFdoZWVsKCkge1xuICAgIGNvbnNvbGUubG9nKCdfYXR0YWNoV2hlZWwnLCB0aGlzKTtcbiAgICBpZiAodGhpcy4kd2luZG93LmFkZEV2ZW50TGlzdGVuZXIpIHtcbiAgICAgIGlmICgnb253aGVlbCcgaW4gZG9jdW1lbnQpIHtcbiAgICAgICAgLy8gSUU5KywgRkYxNyssIENoMzErXG4gICAgICAgIHRoaXMuJHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwid2hlZWxcIiwgdGhpcy5fc2Nyb2xsLmJpbmQodGhpcykpO1xuICAgICAgfSBlbHNlIGlmICgnb25tb3VzZXdoZWVsJyBpbiBkb2N1bWVudCkge1xuICAgICAgICAvLyDRg9GB0YLQsNGA0LXQstGI0LjQuSDQstCw0YDQuNCw0L3RgiDRgdC+0LHRi9GC0LjRj1xuICAgICAgICB0aGlzLiR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNld2hlZWxcIiwgdGhpcy5fc2Nyb2xsLmJpbmQodGhpcykpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gRmlyZWZveCA8IDE3XG4gICAgICAgIHRoaXMuJHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwiTW96TW91c2VQaXhlbFNjcm9sbFwiLCB0aGlzLl9zY3JvbGwuYmluZCh0aGlzKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHsgLy8gSUU4LVxuICAgICAgdGhpcy4kd2luZG93LmF0dGFjaEV2ZW50KFwib25tb3VzZXdoZWVsXCIsIHRoaXMuX3Njcm9sbC5iaW5kKHRoaXMpKTtcbiAgICB9XG4gIH1cblxuICBfcmVtb3ZlV2hlZWwoKSB7XG4gICAgY29uc29sZS5sb2coJ19yZW1vdmVXaGVlbCcsIHRoaXMpO1xuICAgIGlmICh0aGlzLiR3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcikge1xuICAgICAgaWYgKCdvbndoZWVsJyBpbiBkb2N1bWVudCkge1xuICAgICAgICAvLyBJRTkrLCBGRjE3KywgQ2gzMStcbiAgICAgICAgdGhpcy4kd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJ3aGVlbFwiLCB0aGlzLl9zY3JvbGwuYmluZCh0aGlzKSk7XG4gICAgICB9IGVsc2UgaWYgKCdvbm1vdXNld2hlZWwnIGluIGRvY3VtZW50KSB7XG4gICAgICAgIC8vINGD0YHRgtCw0YDQtdCy0YjQuNC5INCy0LDRgNC40LDQvdGCINGB0L7QsdGL0YLQuNGPXG4gICAgICAgIHRoaXMuJHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2V3aGVlbFwiLCB0aGlzLl9zY3JvbGwuYmluZCh0aGlzKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBGaXJlZm94IDwgMTdcbiAgICAgICAgdGhpcy4kd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJNb3pNb3VzZVBpeGVsU2Nyb2xsXCIsIHRoaXMuX3Njcm9sbC5iaW5kKHRoaXMpKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgeyAvLyBJRTgtXG4gICAgICB0aGlzLiR3aW5kb3cuZGV0YWNoRXZlbnQoXCJvbm1vdXNld2hlZWxcIiwgdGhpcy5fc2Nyb2xsLmJpbmQodGhpcykpO1xuICAgIH1cbiAgfVxuXG4gIF9zY3JvbGwoZSkge1xuICAgIGNvbnNvbGUubG9nKCdzY3JvbGwnKTtcbiAgICBmdW5jdGlvbiBoYW5kbGUoZGVsdGEsIHRhcmdldCkge1xuICAgICAgdmFyIHRvcCA9IHRhcmdldC5zY3JvbGxUb3AoKSAtIGRlbHRhO1xuICAgICAgdGFyZ2V0LnNjcm9sbFRvcCh0b3ApO1xuICAgIH1cblxuICAgIGUgPSBlIHx8IHdpbmRvdy5ldmVudDtcblxuICAgIHZhciBkZWx0YSA9IGUuZGVsdGFZIHx8IGUuZGV0YWlsIHx8IGUud2hlZWxEZWx0YTtcblxuICAgIGhhbmRsZShkZWx0YSwgdGhpcy4kZWwpO1xuXG4gICAgZS5wcmV2ZW50RGVmYXVsdCA/IGUucHJldmVudERlZmF1bHQoKSA6IChlLnJldHVyblZhbHVlID0gZmFsc2UpO1xuICB9XG59XG5tb2R1bGUuZXhwb3J0cyA9IFByb2plY3QiLCJjbGFzcyBQcm9qZWN0VmlkZW8ge1xuICBjb25zdHJ1Y3RvcigkZWwpIHtcbiAgICB0aGlzLiRlbCA9ICRlbFxuXG4gICAgdGhpcy5pc1BsYXlpbmcgPSB0cnVlXG5cbiAgICBpZiAod2luZG93LmlzTW9iaWxlKSB7XG4gICAgICB0aGlzLiRlbC5zZXRBdHRyaWJ1dGUoJ2NvbnRyb2xzJywgJ2NvbnRyb2xzJylcbiAgICB9XG5cbiAgICB0aGlzLl9iaW5kRXZlbnRzKClcbiAgfVxuXG4gIF9iaW5kRXZlbnRzKCkge1xuICAgIHRoaXMuJGVsLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlb3ZlcicsIHRoaXMuX3Nob3dDb250cm9scy5iaW5kKHRoaXMpKVxuICAgIHRoaXMuJGVsLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlb3V0JywgdGhpcy5faGlkZUNvbnRyb2xzLmJpbmQodGhpcykpXG4gICAgdGhpcy4kZWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLl9wbGF5UGF1c2UuYmluZCh0aGlzKSlcbiAgfVxuXG4gIF9wbGF5UGF1c2UoKSB7XG4gICAgdGhpcy5pc1BsYXlpbmcgPyB0aGlzLiRlbC5wYXVzZSgpIDogdGhpcy4kZWwucGxheSgpXG4gICAgdGhpcy5pc1BsYXlpbmcgPSAhdGhpcy5pc1BsYXlpbmdcbiAgfVxuXG4gIF9zaG93Q29udHJvbHMoKSB7XG4gICAgdGhpcy4kZWwuY29udHJvbHMgPSB0cnVlXG4gIH1cblxuICBfaGlkZUNvbnRyb2xzKCkge1xuICAgIHRoaXMuJGVsLmNvbnRyb2xzID0gZmFsc2VcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IFByb2plY3RWaWRlb1xuIiwiY29uc3QgQ0xBU1NfU0NBTk5FRCA9ICdpc19fc2Nhbm5lZCdcbmNvbnN0IENMQVNTX0NMT05FID0gJ3NjYW4tY2xvbmUnXG5jb25zdCBDTE9ORVNfTlVNQkVSID0gNVxuXG5jbGFzcyBDYXJkIHtcbiAgY29uc3RydWN0b3IoJGVsKSB7XG4gICAgdGhpcy4kZWwgPSAkZWxcblxuICAgIHRoaXMuX2J1aWxkQ2xvbmVzKClcbiAgfVxuXG4gIF9idWlsZENsb25lcygpIHtcbiAgICBjb25zdCB0ZXh0ID0gdGhpcy4kZWwuaW5uZXJUZXh0XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBDTE9ORVNfTlVNQkVSOyBpKyspIHtcbiAgICAgIGNvbnN0IGNsb25lID0gdGhpcy4kZWwuY2xvbmVOb2RlKClcbiAgICAgIGNsb25lLnJlbW92ZUF0dHJpYnV0ZSgnaWQnKVxuICAgICAgY2xvbmUuY2xhc3NMaXN0LnJlbW92ZSgnc2NhbicpXG4gICAgICBjbG9uZS5jbGFzc0xpc3QucmVtb3ZlKCdqc19fc2NhbicpXG4gICAgICBjbG9uZS5jbGFzc0xpc3QuYWRkKENMQVNTX0NMT05FKVxuICAgICAgY2xvbmUuaW5uZXJUZXh0ID0gdGV4dFxuICAgICAgdGhpcy4kZWwuYXBwZW5kQ2hpbGQoY2xvbmUpXG4gICAgfVxuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gQ2FyZFxuIiwiY2xhc3MgU2Nyb2xsZXIge1xuICBjb25zdHJ1Y3RvcigkZWwpIHtcbiAgICB0aGlzLiRlbCA9ICRlbFxuXG4gICAgdGhpcy5fYmluZEV2ZW50cygpXG4gIH1cblxuICBfYmluZEV2ZW50cygpIHtcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCd3aGVlbCcsIGUgPT4ge1xuICAgICAgdGhpcy4kZWwuc2Nyb2xsVG9wIC09IGUud2hlZWxEZWx0YVk7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgICB9KVxuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gU2Nyb2xsZXJcbiIsImNvbnN0IFJFU0laRV9FVkVOVCA9IHdpbmRvdy5pc01vYmlsZSA/ICdvcmllbnRhdGlvbmNoYW5nZScgOiAncmVpemUnXG5jb25zdCBDTE9ORVNfTlVNQkVSID0gNTtcblxuY2xhc3MgU2Nyb2xsaW5nVGV4dCB7XG4gIGNvbnN0cnVjdG9yKCRlbCkge1xuICAgIHRoaXMuJGVsID0gJGVsXG4gICAgdGhpcy4kaW4gPSB0aGlzLiRlbC5maXJzdEVsZW1lbnRDaGlsZFxuICAgIHRoaXMuJHRleHQgPSB0aGlzLiRpbi5maXJzdEVsZW1lbnRDaGlsZFxuXG4gICAgdGhpcy5jbG9uZXMgPSBbXVxuICAgIHRoaXMuZHVyYXRpb24gPSB0aGlzLiRlbC5kYXRhc2V0LmR1cmF0aW9uICogQ0xPTkVTX05VTUJFUlxuICAgIHRoaXMuaXNJbnZlcnNlZCA9IHRoaXMuJGVsLmRhdGFzZXQuaW52ZXJzZVxuXG4gICAgdGhpcy5fYnVpbGRTdHlsZVRhZygpXG4gICAgdGhpcy5fYmluZEV2ZW50cygpXG4gICAgdGhpcy5faW5pdCgpXG4gIH1cblxuICBfYnVpbGRTdHlsZVRhZygpIHtcbiAgICB0aGlzLiRzdHlsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0eWxlJylcbiAgICBkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKHRoaXMuJHN0eWxlKVxuICB9XG5cbiAgX2luaXQoKSB7XG4gICAgY29uc3Qgd2luZG93V2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aFxuICAgIHRoaXMud2lkdGggPSB0aGlzLiRpbi5jbGllbnRXaWR0aFxuXG4gICAgaWYgKHRoaXMud2lkdGgpIHtcbiAgICAgIGNvbnN0IGNsb25lc051bWJlciA9IE1hdGgucm91bmQod2luZG93V2lkdGggLyB0aGlzLndpZHRoKSArIDFcbiAgICAgIHRoaXMub3ZlclNjcm9sbCA9IGNsb25lc051bWJlciAqIHRoaXMud2lkdGggLSB3aW5kb3dXaWR0aFxuXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNsb25lc051bWJlcjsgaSsrKSB7XG4gICAgICAgIGNvbnN0ICRjbG9uZSA9IHRoaXMuJHRleHQuY2xvbmVOb2RlKHRydWUpXG5cbiAgICAgICAgdGhpcy5jbG9uZXMucHVzaCgkY2xvbmUpXG4gICAgICAgIHRoaXMuJGluLmFwcGVuZENoaWxkKCRjbG9uZSlcbiAgICAgIH1cblxuICAgICAgdGhpcy5od2lkdGggPSB0aGlzLiRpbi5jbGllbnRXaWR0aCAqIENMT05FU19OVU1CRVJcblxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBDTE9ORVNfTlVNQkVSOyBpKyspIHtcbiAgICAgICAgY29uc3QgJGNsb25lID0gdGhpcy4kaW4uY2xvbmVOb2RlKHRydWUpXG4gICAgICAgIHRoaXMuY2xvbmVzLnB1c2goJGNsb25lKVxuICAgICAgICB0aGlzLiRlbC5hcHBlbmRDaGlsZCgkY2xvbmUpXG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5fYnVpbGRLZXlmcmFtZSgpXG4gIH1cblxuICBfYnVpbGRLZXlmcmFtZSgpIHtcbiAgICBjb25zdCBpZCA9IGBpZCR7KG5ldyBEYXRlKCkpLmdldFRpbWUoKX1gXG5cbiAgICB0aGlzLiRlbC5pZCA9IGlkXG5cbiAgICBjb25zdCBmcm9tID0gdGhpcy5pc0ludmVyc2VkID8gLXRoaXMuaHdpZHRoIDogMFxuICAgIGNvbnN0IHRpbGwgPSB0aGlzLmlzSW52ZXJzZWQgPyAwIDogLXRoaXMuaHdpZHRoXG5cbiAgICBjb25zdCBzdHlsZSA9IGBcbiAgICAgIEBrZXlmcmFtZXMgc2Nyb2xsLSR7aWR9IHtcbiAgICAgICAwJSB7IHRyYW5zZm9ybTogdHJhbnNsYXRlWCgke2Zyb219cHgpIHRyYW5zbGF0ZVooMCk7IH1cbiAgICAgICAxMDAlIHsgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKCR7dGlsbH1weCkgdHJhbnNsYXRlWigwKTsgfVxuICAgICAgfVxuXG4gICAgICAjJHtpZH0ge1xuICAgICAgICBhbmltYXRpb246IHNjcm9sbC0ke2lkfSAke3RoaXMuZHVyYXRpb259cyBsaW5lYXIgaW5maW5pdGU7XG4gICAgICB9XG4gICAgYFxuXG4gICAgdGhpcy4kc3R5bGUuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoc3R5bGUpKTtcbiAgfVxuXG4gIF9yZWluaXQoKSB7XG4gICAgdGhpcy5fcmVtb3ZlQ2xvbmVzKClcbiAgICB0aGlzLl9pbml0KClcbiAgfVxuXG4gIF9yZW1vdmVDbG9uZXMoKSB7XG4gICAgdGhpcy5jbG9uZXMuZm9yRWFjaCgkY2xvbmUgPT4gJGNsb25lLnJlbW92ZSgpKVxuICAgIHRoaXMuY2xvbmVzID0gW11cbiAgfVxuXG4gIF9iaW5kRXZlbnRzKCkge1xuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFJFU0laRV9FVkVOVCwgdGhpcy5fcmVpbml0LmJpbmQodGhpcykpXG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBTY3JvbGxpbmdUZXh0XG4iLCJjb25zdCBDTEFTU19QTEFZSU5HID0gJ2lzX19wbGF5aW5nJ1xuXG5jbGFzcyBWaWRlbyB7XG4gIGNvbnN0cnVjdG9yKCRlbCkge1xuICAgIHRoaXMuJGVsID0gJGVsXG5cbiAgICB0aGlzLl9iaW5kRXZlbnRzKClcbiAgfVxuXG4gIF9iaW5kRXZlbnRzKCkge1xuICAgIHRoaXMuJGVsLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlb3ZlcicsIHRoaXMuX3BsYXkuYmluZCh0aGlzKSlcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW91dCcsIHRoaXMuX3BhdXNlLmJpbmQodGhpcykpXG4gICAgdGhpcy4kZWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBlID0+IGUucHJldmVudERlZmF1bHQoKSlcbiAgfVxuXG4gIF9wbGF5KCkge1xuICAgIHRoaXMuJGVsLnBsYXkoKVxuICAgIHRoaXMuJGVsLmNsYXNzTGlzdC5hZGQoQ0xBU1NfUExBWUlORylcbiAgfVxuXG4gIF9wYXVzZSgpIHtcbiAgICB0aGlzLiRlbC5wYXVzZSgpXG4gICAgdGhpcy4kZWwuY2xhc3NMaXN0LnJlbW92ZShDTEFTU19QTEFZSU5HKVxuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gVmlkZW9cbiJdfQ==
