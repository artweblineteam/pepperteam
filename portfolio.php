<?php /* Template Name: Portfolio */ ?>

<?php get_header(); ?>

<?php
$image_projects = get_posts(array('post_type'=> 'imageproject', 'numberposts' => -1));
$video_projects = get_posts(array('post_type'=> 'videoproject', 'numberposts' => -1));

$projects = array();


for ($i = 0; $i < max(count($image_projects), count($video_projects)); $i++) {
  if ($video_projects[$i]) {
    $projects[] = $video_projects[$i];
  }

  if ($image_projects[$i]) {
    $projects[] = $image_projects[$i];
  }
}

?>

<div class="portfolioBox js__portfolio">

  <a class="back is__main" href="/"><———</a>

  <div class="portfolio is__ready">
    <?php foreach ($projects as $project) { ?>
      <a class="portfolio-card card js__portfolio-item js__card is__<?php echo $project->post_type ?>" href="#project-<?php echo $project->ID ?>" style="margin-top: <?php echo get_field('top', $project->ID); ?>%; margin-left: <?php echo get_field('left', $project->ID); ?>%;">
        <h2 class="card-title"><?php echo $project->post_title ?></h2>
        <?php if ($project->post_type == 'videoproject') { ?>
          <?php
            $video_url = get_field('video_preview', $project->ID);
            if (!$video_url) $video_url = get_field('video', $project->ID);
          ?>
          <video class="video js__video" nocontrols muted>
            <source src="<?php echo $video_url?>" type="video/mp4">
          </video>
        <?php } else { ?>
          <div class="card-image scan js__scan">
            <?php echo get_field('abbr', $project->ID) ?>
          </div>
        <?php } ?>
      </a>
    <?php } ?>
  </div>

  <div class="portfolioSelector">
    <a class="portfolioSelector-item is__photo js__portfolioSelector" href="#photo">Photo</a>
    <div class="portfolioSelector-divider"></div>
    <a class="portfolioSelector-item is__video js__portfolioSelector" href="#video">Video</a>
  </div>

  <div class="portfolioSingle is__video">
    <?php foreach ($video_projects as $project) { ?>
      <a class="portfolioSingle-item js__card is__<?php echo $project->post_type ?>" href="#project-<?php echo $project->ID ?>">
        <?php echo $project->post_title ?>
      </a>
    <?php } ?>
    <a class="back js__portfolio-back" href="/"><———</a>
  </div>

  <div class="portfolioSingle is__photo">
    <?php foreach ($image_projects as $project) { ?>
      <a class="portfolioSingle-item js__card is__<?php echo $project->post_type ?>" href="#project-<?php echo $project->ID ?>">
        <?php echo $project->post_title ?>
      </a>
    <?php } ?>
    <?php foreach ($image_projects as $project) { ?>
      <a class="portfolioSingle-item js__card is__<?php echo $project->post_type ?>" href="#project-<?php echo $project->ID ?>">
        <?php echo $project->post_title ?>
      </a>
    <?php } ?>
    <a class="back js__portfolio-back" href="/"><———</a>
  </div>

  <div class="projects">
    <?php foreach ($projects as $project) { ?>
      <div class="project js__project" id="project-<?php echo $project->ID ?>">
        <?php
          $place_header_after_media = get_field('place_header_after_media', $project->ID);
          $header_style = 'margin-left: '.get_field('header_offset', $project->ID).'%;';
          $media_style = 'margin-left: '.get_field('media_offset', $project->ID).'%;';
          $text_style = 'margin-left: '.get_field('text_offset', $project->ID).'%;';
        ?>
        <div class="project-content">
          <button class="back js__project-close" type="button"></button>
          <?php if (!$place_header_after_media) : ?>
            <h2 class="project-title" style="<?php echo $header_style ?>"><?php echo $project->post_title ?></h2>
          <?php endif ?>

          <div class="project-media" style="<?php echo $media_style ?>">
            <?php if ($project->post_type == 'videoproject') { ?>
              <video class="project-video js__projectVideo" nocontrols poster="<?php echo get_field('cover', $project->ID) ?>">
                <source src="<?php echo get_field('video', $project->ID)?>" type="video/mp4">
              </video>
            <?php } else { ?>
              <div class="smart-slider">
                <div class="smart-slider-clip">
                  <div class="smart-shaft" data-allowfullscreen="native" data-loop="true">
                    <?php $images = get_field('image', $project->ID)?>
                    <?php foreach ($images as $image) { ?>
          
                        <!-- <div class="fotorama-image" style="background-image: url(<?php //echo $image['url'] ?>)"></div> -->
                        
                          <img src="<?php echo $image['url'] ?>" class="smart-slider-image">
                        
                
                    <?php } ?>
                  </div>
                </div>

               <!--  <a href="#" class="fotorama-nav is__prev"></a>
                <a href="#" class="fotorama-nav is__next"></a> -->
              </div>
            <?php } ?>
          </div>

          <?php if ($place_header_after_media) : ?>
            <h2 class="project-title" style="<?php echo $header_style ?>"><?php echo $project->post_title ?></h2>
          <?php endif ?>

          <div class="project-description" style="<?php echo $text_style ?>"><?php echo $project->post_content ?></div>
        </div>
      </div>
    <?php } ?>
  </div>
</div>

<?php get_footer();
