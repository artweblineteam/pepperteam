<?php /* Template Name: Info Third */ ?>

<?php get_header(); ?>

<div class="info is__static" id="second">
  <a class="info-closer js__closer" href="/"></a>
  <div class="info-box js__scroller">
    <div class="info-box-col"><?php echo get_field('info_third'); ?></div>
    <div class="info-box-col"><?php //echo get_field('column_2'); ?></div>
  </div>
</div>

<?php get_footer();
