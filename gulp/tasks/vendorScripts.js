const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

module.exports = () => {
  return gulp.src('source/scripts/vendor/**')
    .pipe($.changed('build'))
    .pipe(gulp.dest('build/scripts/vendor'));
};
