const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

module.exports = () => {
  return gulp.src('source/fonts/**')
    .pipe($.changed('build'))
    .pipe(gulp.dest('build/fonts'));
};
