const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

module.exports = () => {
  return gulp.src('source/images/**')
    .pipe($.changed('build'))
    .pipe(gulp.dest('build/images'));
};
