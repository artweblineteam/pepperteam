const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const bsync = require('browser-sync');
const isProduction = require('../helpers/envHelpers').isProduction;
const errorHandler = require('../helpers/errorHandler');

module.exports = function() {
  return gulp.src('source/styles/app.css')
    .pipe($.if(!isProduction, $.sourcemaps.init()))
    .pipe($.postcss([
      require('postcss-easy-import')({
        extensions: ['.css', '.pcss'],
      }),
      require('postcss-for'),
      require('postcss-each'),
      require('postcss-mixins'),
      require('postcss-conditionals'),
      require('postcss-simple-vars'),
      require('postcss-calc')({ mediaQueries: true }),
      require('postcss-nested'),
      require('postcss-assets'),
      require('autoprefixer')({ browsers: ['last 2 version'] }),
    ]))
    .on('error', errorHandler)
    .pipe($.if(!isProduction, $.sourcemaps.write()))
    .pipe(gulp.dest('build/styles'))
    .pipe(bsync.reload({ stream: true }));
};
