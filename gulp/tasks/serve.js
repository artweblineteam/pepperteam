const gulp = require('gulp');
const bsync = require('browser-sync');
const $ = require('gulp-load-plugins')();

module.exports = () => {
  bsync({
    notify: false,
    port: 9000,
    open: false,
    ghostMode: false,
    proxy: 'pepperteam.dev'
  });

  gulp.watch(['source/styles/**/*.pcss', 'source/styles/**/*.css'], ['styles']);
  gulp.watch(['source/images/**'], ['images']);;
  gulp.watch(['source/fonts/**'], ['fonts']);;

  gulp.watch([
    '*.php',
    '**/*.php',
    'build/images/**',
    'build/fonts/**',
  ], bsync.reload);;
}
