<?php /* Template Name: Main */ ?>

<?php get_header(); ?>

<?php //$about = get_page_by_path('about', OBJECT); ?>
<?php $info_1 = get_page_by_path('info-1', OBJECT ); ?>
<?php $info_2 = get_page_by_path('info-2', OBJECT); ?>
<?php $info_3 = get_page_by_path('info-3', OBJECT); ?>


<div class="index">
<?php
      the_content(); ?>
<div class="custom-lang-sw">
  <?php if ( is_active_sidebar( 'custom-side-bar' ) ) : ?>
      <?php dynamic_sidebar( 'custom-side-bar' ); ?>
  <?php endif; ?>
</div>
<div class="logoBox">
  <div class="logo">
    <h1 class="logo-lead">PepperTeam</h1>
    <div class="logo-text">Technical <br>production <br>around <br>the world.</div>
    <div class="logo-main">
      <div class="logo-twin"></div>
      <div class="logo-dot js__dot is__first">
        <a class="logo-dot-link" href="/info-1"></a>
        <div class="info" id="first">
          <div class="info-closer js__closer"></div>
          <div class="info-box js__scroller">
            <div class="info-box-col"><?php echo get_field('info_first'); ?>
            </div>
            <div class="info-box-col"><?php //echo get_field('column_2', $info_1->ID); ?></div>
          </div>
        </div>
      </div>
      <div class="logo-dot js__dot is__second">
        <a class="logo-dot-link" href="/info-2"></a>
        <div class="info" id="second">
          <div class="info-closer js__closer"></div>
          <div class="info-box js__scroller">
            <div class="info-box-col"><?php echo get_field('info_second'); ?></div>
            <div class="info-box-col"><?php //echo get_field('column_2', $info_2->ID); ?></div>
          </div>
        </div>
      </div>
      <div class="logo-dot js__dot is__third">
        <a class="logo-dot-link" href="/info-3"></a>
        <div class="info" id="third">
          <div class="info-closer js__closer"></div>
          <div class="info-box js__scroller">
            <div class="info-box-col"><?php echo get_field('info_third'); ?></div>
            <div class="info-box-col"><?php //echo get_field('column_2', $info_3->ID); ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="portfolioLinkBox">
  <div class="portfolioLink">
    <a class="portfolioLink-link scan is__far js__scan" href="portfolio/"></a> 
  </div>
</div>

<?php
  $fb_line = 'FB:/'.get_field('facebook_name', $about->ID);
  $ig_line = 'IG:/'.get_field('instagram_name', $about->ID);

  $fb = '
    <a class="contact" href="http://facebook.com/'.get_field('facebook_name', $about->ID).'" target="_blank">
      <div class="contact-label">FB</div>
      <div class="contact-popup">
        <div class="contact-bubble">
          '.$fb_line.'<br>
          '.$fb_line.'<br>
          '.$fb_line.'
        </div>
        <div class="contact-shadow"></div>
      </div>
    </a>
  ';

  $ig = '
    <a class="contact" href="http://instagram.com/'.get_field('instagram_name', $about->ID).'" target="_blank">
      <div class="contact-label">IG</div>
      <div class="contact-popup">
        <div class="contact-bubble">
          '.$ig_line.'<br>
          '.$ig_line.'<br>
          '.$ig_line.'
        </div>
        <div class="contact-shadow"></div>
      </div>
    </a>
  ';

  $whatsapp = '
    <a class="contact" href="https://wa.me/'.get_field('whatsapp', $about->ID).'" target="_blank">
      <div class="contact-label">whatsapp</div>
    </a>
  ';  

  $fb_single = '<a class="contact-phone" href="http://facebook.com/'.get_field('facebook_name', $about->ID).'" target="_blank">'.$fb_line.'</a>';
  $ig_single = '<a class="contact-phone" href="http://instagram.com/'.get_field('instagram_name', $about->ID).'" target="_blank">'.$ig_line.'</a>';
  $phone = '<div class="contacts-phone">'.get_field('phone', $about->ID).'</div>';
?>
<div class="contacts">

  <div class="contacts-scroller js__scrollingText is__desk" data-duration="40">
    <div class="contacts-scroller-in">
      <div class="contacts-holder">
        <?php echo $phone ?>
        <?php echo $fb ?>
        <?php echo $phone ?>
        <?php echo $ig ?>
      </div>
    </div>
  </div>

  <div class="contacts-scroller js__scrollingText is__desk whatsapp" data-duration="30">
    <div class="contacts-scroller-in">
      <div class="contacts-holder">
        <?php echo $whatsapp ?>
      </div>
    </div>
  </div>

  <div class="contacts-scroller js__scrollingText is__mobile" data-duration="50">
    <div class="contacts-scroller-in">
      <div class="contacts-holder">
        <?php echo $phone ?>
      </div>
    </div>
  </div>

  <div class="contacts-scroller js__scrollingText is__mobile" data-duration="55" data-inverse="1">
    <div class="contacts-scroller-in">
      <div class="contacts-holder">
        <?php echo $fb_single ?>
      </div>
    </div>
  </div>

  <div class="contacts-scroller js__scrollingText is__mobile" data-duration="60">
    <div class="contacts-scroller-in">
      <div class="contacts-holder">
        <?php echo $ig_single ?>
      </div>
    </div>
  </div>

  <div class="contacts-scroller js__scrollingText is__mobile whatsapp" data-duration="65" data-inverse="1">
    <div class="contacts-scroller-in">
      <div class="contacts-holder">
        <?php echo $whatsapp ?>
      </div>
    </div>
  </div>

</div>

<div class="mapWrapper">
<div class="map">
  <div class="map-address">
    <div class="map-marker"></div>
    <div class="map-popup">
      <?php echo get_post_meta($post->ID, "address", true); ?>
    </div>
  </div>

  <div class="man">
    <!-- <div style="left: 0px; top: 0px; width: 26px; height: 50px; z-index: 105;"> -->
      <div>
      <img src="<?php  echo get_stylesheet_directory_uri(); ?>/build/images/man.gif" style="opacity: 1; width: 26px;">
    </div>
  </div>

  <div class="car" >
      <div style="left: 0px; top: 26.3593px; width: 44px;  z-index: 103; transform: rotateX(0deg) rotateY(0deg) rotateZ(-89deg) scaleX(-1);"> 
        <img src="<?php  echo get_stylesheet_directory_uri(); ?>/build/images/car.png">
      </div>
    </div>


<div class="new-map">
  <img src="<?php  echo get_stylesheet_directory_uri(); ?>/build/images/newmap.svg">
</div>

<div class="new-map-mob">
  <img src="<?php  echo get_stylesheet_directory_uri(); ?>/build/images/newmapmob.svg" width="100%" height="100%">
</div>


</div>
</div>
</div>

<?php get_footer();
