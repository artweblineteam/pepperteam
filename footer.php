<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pepperteam
 */

?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri() ?>/source/scripts/owl.carousel.min.js"></script>
    <script async src="<?php echo get_stylesheet_directory_uri() ?>/build/scripts/app.js?v=1"></script>
<?php wp_footer(); ?>

</body>
</html>
