if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
  window.isMobile = true
  document.documentElement.classList.add('is__mobile')

  const setOrientation = () => {
    if (window.innerHeight < window.innerWidth) {
      document.documentElement.classList.add('is__horizontal')
    } else {
      document.documentElement.classList.remove('is__horizontal')
    }
  }

  window.addEventListener('orientationchange', setOrientation)
  setOrientation()
}

document.documentElement.classList.add('is__ready')

if (location.pathname.split('/').filter(Boolean).pop() === 'en') {
  const links = [].slice.call(document.querySelectorAll('.portfolioLink-link, .logo-dot-link'))
  links.forEach(link => link.href && (link.href = 'en/' + link.href))
}



import Video from './app/video'
import Project from './app/project'
import Card from './app/card'
import Scan from './app/scan'
import Scroller from './app/scroller'
//import Fotorama from './app/fotorama'
import Dot from './app/dot'
import Closer from './app/closer'
import ScrollingText from './app/scrollingText'
import ProjectVideo from './app/projectVideo'
import PortfolioSelector from './app/portfolioSelector'
import Portfolio from './app/portfolio'

{
  document.querySelectorAll('.js__video').forEach(el => new Video(el))
  document.querySelectorAll('.js__project').forEach(el => new Project(el))
  document.querySelectorAll('.js__card').forEach(el => new Card(el))
  document.querySelectorAll('.js__scan').forEach(el => new Scan(el))
  document.querySelectorAll('.js__scroller').forEach(el => new Scroller(el))
  //document.querySelectorAll('.js_slickslider').forEach(el => new SlickSlider(el))
  document.querySelectorAll('.js__dot').forEach(el => new Dot(el))
  document.querySelectorAll('.js__closer').forEach(el => new Closer(el))
  document.querySelectorAll('.js__scrollingText').forEach(el => new ScrollingText(el))
  document.querySelectorAll('.js__projectVideo').forEach(el => new ProjectVideo(el))
  document.querySelectorAll('.js__portfolioSelector').forEach(el => new PortfolioSelector(el))
  document.querySelectorAll('.js__portfolio').forEach(el => new Portfolio(el))
}

$(function () {
  $('.smart-shaft').fotorama();
});

$(function () {
  $('.team-carousel').fotorama();
});

// $(function () {
//   $('.team-carousel').owlCarousel({
//     loop:true,
//     margin:10,
//     nav:true,
// });
// });

