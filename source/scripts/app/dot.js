const CLASS_ACTIVE = 'is__active'
const CLASS_POPUP = 'has__popup'

class Dot {
  constructor($el) {
    this.$el = $el

    this._bindEvents()
  }

  _bindEvents() {
    this.$el.addEventListener('click', e => {
      e.stopPropagation()
      document.body.classList.add(CLASS_POPUP)
      this.$el.classList.add(CLASS_ACTIVE)

      document.dispatchEvent(new CustomEvent('closePopups', { detail: { $el: this.$el }}))
    })

    document.addEventListener('click', _ => {
      this.$el.classList.remove(CLASS_ACTIVE)
      document.body.classList.remove(CLASS_POPUP)
    })

    document.addEventListener('closePopups', (e, data) => {
      if (!e.detail || e.detail.$el !== this.$el) {
        this.$el.classList.remove(CLASS_ACTIVE)
      }

      if (!e.detail) {
        document.body.classList.remove(CLASS_POPUP)
      }
    })
  }
}

module.exports = Dot
