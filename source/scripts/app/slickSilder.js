class SlickSilder {
$('.smart-shaft').slick({
  infinite        : true,
  slidesToShow    : 1,
  slidesToScroll  : 1,
  //mobileFirst     : true,
  arrows:true,
});

$('.smart-shaft').slickLightbox({
  itemSelector        : 'a',
  navigateByKeyboard  : true
});
}

module.export = SlickSilder