class ProjectVideo {
  constructor($el) {
    this.$el = $el

    this.isPlaying = true

    if (window.isMobile) {
      this.$el.setAttribute('controls', 'controls')
    }

    this._bindEvents()
  }

  _bindEvents() {
    this.$el.addEventListener('mouseover', this._showControls.bind(this))
    this.$el.addEventListener('mouseout', this._hideControls.bind(this))
    this.$el.addEventListener('click', this._playPause.bind(this))
  }

  _playPause() {
    this.isPlaying ? this.$el.pause() : this.$el.play()
    this.isPlaying = !this.isPlaying
  }

  _showControls() {
    this.$el.controls = true
  }

  _hideControls() {
    this.$el.controls = false
  }
}

module.exports = ProjectVideo
