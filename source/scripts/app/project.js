const CLASS_OPENED = 'is__opened'
const CLASS_FADED = 'is__faded'

class Project {
  constructor($el) {
    this.$el = $el
    this.$body = document.body
    this.$window = window 
    this.$content = this.$el.children[0]
    this.$video = this.$content.querySelector('video')
    this.$close = this.$content.querySelector('.js__project-close')

    this.baseTitle = document.title
    this.title = this.$el.querySelector('h2').innerText

    this.id = this.$el.id

    if (window.location.hash === `#${this.id}`) {
      this._open()
    }

    this._bindEvents()
  }

  _bindEvents() {
    this.$el.addEventListener('open', this._open.bind(this))
    this.$el.addEventListener('close', this._close.bind(this))
    this.$el.addEventListener('click', this._close.bind(this))
    this.$close.addEventListener('click', this._close.bind(this))
    this.$content.childNodes.forEach(child => {
      child.addEventListener('click', e => e.stopPropagation())
    })
    document.addEventListener('keydown', e => {
      if (e.keyCode === 27) this._close()
    })
  }

  _open() {
    console.log("open");
    this.$el.classList.add(CLASS_OPENED)
    this.$body.classList.add(CLASS_FADED)
    document.title = this.title
    this._attachWheel.bind(this);

    if (this.$video && !window.isMobile) {
      this.$video.play()
    }

    history.pushState(null, this.title, `#${this.id}`);
    
  }

  _close() {
    console.log("close");
    this.$el.classList.remove(CLASS_OPENED)
    this.$body.classList.remove(CLASS_FADED)
    document.title = this.baseTitle
    history.pushState(null, this.baseTitle, window.location.pathname);
    this._removeWheel.bind(this);

    if (this.$video && !window.isMobile) {
      this.$video.pause()
    }
    

    document.dispatchEvent(new CustomEvent('portfolioFilter', { 'detail': window.prevType }))
  }

  _attachWheel() {
    console.log('_attachWheel', this);
    if (this.$window.addEventListener) {
      if ('onwheel' in document) {
        // IE9+, FF17+, Ch31+
        this.$window.addEventListener("wheel", this._scroll.bind(this));
      } else if ('onmousewheel' in document) {
        // устаревший вариант события
        this.$window.addEventListener("mousewheel", this._scroll.bind(this));
      } else {
        // Firefox < 17
        this.$window.addEventListener("MozMousePixelScroll", this._scroll.bind(this));
      }
    } else { // IE8-
      this.$window.attachEvent("onmousewheel", this._scroll.bind(this));
    }
  }

  _removeWheel() {
    console.log('_removeWheel', this);
    if (this.$window.removeEventListener) {
      if ('onwheel' in document) {
        // IE9+, FF17+, Ch31+
        this.$window.removeEventListener("wheel", this._scroll.bind(this));
      } else if ('onmousewheel' in document) {
        // устаревший вариант события
        this.$window.removeEventListener("mousewheel", this._scroll.bind(this));
      } else {
        // Firefox < 17
        this.$window.removeEventListener("MozMousePixelScroll", this._scroll.bind(this));
      }
    } else { // IE8-
      this.$window.detachEvent("onmousewheel", this._scroll.bind(this));
    }
  }

  _scroll(e) {
    console.log('scroll');
    function handle(delta, target) {
      var top = target.scrollTop() - delta;
      target.scrollTop(top);
    }

    e = e || window.event;

    var delta = e.deltaY || e.detail || e.wheelDelta;

    handle(delta, this.$el);

    e.preventDefault ? e.preventDefault() : (e.returnValue = false);
  }
}
module.exports = Project