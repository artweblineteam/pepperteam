class Card {
  constructor($el) {
    this.$el = $el
    this.target = document.querySelector(this.$el.getAttribute('href'))

    this._bindEvents()
  }

  _bindEvents() {
    this.$el.addEventListener('click', e => {
      e.preventDefault();
      this.target.dispatchEvent(new Event('open'))
    })
  }
}

module.exports = Card
