class Fotorama {
  constructor($el) {
    this.$el = $el
    this.$shaft = this.$el.querySelector('.fotorama-shaft')
    this.$items = this.$el.querySelectorAll('.fotorama-image')
    this.$navPrev = this.$el.querySelector('.fotorama-nav.is__prev')
    this.$navNext = this.$el.querySelector('.fotorama-nav.is__next')

    this.length = this.$items.length
    this.currentSlide = 0

    this._bindEvents()
  }

  _bindEvents() {
    this.$navPrev.addEventListener('click', e => {
      e.preventDefault()
      this.currentSlide--

      if (this.currentSlide < 0) {
        this.currentSlide = this.length - 1
      }

      this._slide()
    })

    this.$navNext.addEventListener('click', e => {
      e.preventDefault()
      this.currentSlide++

      if (this.currentSlide > this.length - 1) {
        this.currentSlide = 0
      }

      this._slide()
    })
  }

  _slide() {
    this.$shaft.style.transform = `translateX(-${this.currentSlide}%)`
  }
}

module.exports = Fotorama
