const CLASS_PLAYING = 'is__playing'

class Video {
  constructor($el) {
    this.$el = $el

    this._bindEvents()
  }

  _bindEvents() {
    this.$el.addEventListener('mouseover', this._play.bind(this))
    this.$el.addEventListener('mouseout', this._pause.bind(this))
    this.$el.addEventListener('click', e => e.preventDefault())
  }

  _play() {
    this.$el.play()
    this.$el.classList.add(CLASS_PLAYING)
  }

  _pause() {
    this.$el.pause()
    this.$el.classList.remove(CLASS_PLAYING)
  }
}

module.exports = Video
