class Closer {
  constructor($el) {
    this.$el = $el

    this._bindEvents()
  }

  _bindEvents() {
    this.$el.addEventListener('click', e => {
      e.stopPropagation()

      document.dispatchEvent(new CustomEvent('closePopups'))
    })
  }
}

module.exports = Closer
