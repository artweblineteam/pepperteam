class Scroller {
  constructor($el) {
    this.$el = $el

    this._bindEvents()
  }

  _bindEvents() {
    this.$el.addEventListener('wheel', e => {
      this.$el.scrollTop -= e.wheelDeltaY;
      e.preventDefault()
    })
  }
}

module.exports = Scroller
