class Portfolio {
  constructor($el) {
    this.$el = $el
    this.$back = $el.querySelectorAll('.js__portfolio-back')

    this.baseTitle = document.title

    if (window.location.hash) {
      this._hideSelector()

      if (window.location.hash === 'photo' || window.location.hash === 'video') {
        this._filter(window.location.hash)
      }
    }

    this._bindEvents()
  }

  _bindEvents() {
    document.addEventListener('portfolioFilter', e => {
      this._filter(e.detail)
    })

    this.$back.forEach($back => {
      $back.addEventListener('click', e => {
        e.preventDefault()

        this._filter()
      })
    })
  }

  _filter(type) {
    if (type) {
      history.pushState(null, type, `#${type}`)
      window.prevType = type;
      this._clear()
      this.$el.classList.add(`is__${type}`)
      this.currentType = type
      this._hideSelector()
    } else {
      history.pushState(null, this.baseTitle, window.location.pathname)
      delete window.prevType;
      this._clear()
      this._showSelector()
    }
  }

  _hideSelector() {
    this.$el.classList.add('is__hiddenSelector')
  }

  _showSelector() {
    this.$el.classList.remove('is__hiddenSelector')
  }

  _clear() {
    this.$el.classList.remove(`is__${this.currentType}`)
  }
}

module.exports = Portfolio
