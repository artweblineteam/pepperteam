class PortfolioSelector {
  constructor($el) {
    this.$el = $el
    this.target = this.$el.getAttribute('href').slice(1)

    this._bindEvents()
  }

  _bindEvents() {
    this.$el.addEventListener('click', e => {
      e.preventDefault();
      document.dispatchEvent(new CustomEvent('portfolioFilter', { 'detail': this.target }))
    })
  }
}

module.exports = PortfolioSelector
