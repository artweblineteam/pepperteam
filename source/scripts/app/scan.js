const CLASS_SCANNED = 'is__scanned'
const CLASS_CLONE = 'scan-clone'
const CLONES_NUMBER = 5

class Card {
  constructor($el) {
    this.$el = $el

    this._buildClones()
  }

  _buildClones() {
    const text = this.$el.innerText
    for (let i = 0; i < CLONES_NUMBER; i++) {
      const clone = this.$el.cloneNode()
      clone.removeAttribute('id')
      clone.classList.remove('scan')
      clone.classList.remove('js__scan')
      clone.classList.add(CLASS_CLONE)
      clone.innerText = text
      this.$el.appendChild(clone)
    }
  }
}

module.exports = Card
