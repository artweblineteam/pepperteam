const RESIZE_EVENT = window.isMobile ? 'orientationchange' : 'reize'
const CLONES_NUMBER = 5;

class ScrollingText {
  constructor($el) {
    this.$el = $el
    this.$in = this.$el.firstElementChild
    this.$text = this.$in.firstElementChild

    this.clones = []
    this.duration = this.$el.dataset.duration * CLONES_NUMBER
    this.isInversed = this.$el.dataset.inverse

    this._buildStyleTag()
    this._bindEvents()
    this._init()
  }

  _buildStyleTag() {
    this.$style = document.createElement('style')
    document.head.appendChild(this.$style)
  }

  _init() {
    const windowWidth = window.innerWidth
    this.width = this.$in.clientWidth

    if (this.width) {
      const clonesNumber = Math.round(windowWidth / this.width) + 1
      this.overScroll = clonesNumber * this.width - windowWidth

      for (let i = 0; i < clonesNumber; i++) {
        const $clone = this.$text.cloneNode(true)

        this.clones.push($clone)
        this.$in.appendChild($clone)
      }

      this.hwidth = this.$in.clientWidth * CLONES_NUMBER

      for (let i = 0; i < CLONES_NUMBER; i++) {
        const $clone = this.$in.cloneNode(true)
        this.clones.push($clone)
        this.$el.appendChild($clone)
      }
    }

    this._buildKeyframe()
  }

  _buildKeyframe() {
    const id = `id${(new Date()).getTime()}`

    this.$el.id = id

    const from = this.isInversed ? -this.hwidth : 0
    const till = this.isInversed ? 0 : -this.hwidth

    const style = `
      @keyframes scroll-${id} {
       0% { transform: translateX(${from}px) translateZ(0); }
       100% { transform: translateX(${till}px) translateZ(0); }
      }

      #${id} {
        animation: scroll-${id} ${this.duration}s linear infinite;
      }
    `

    this.$style.appendChild(document.createTextNode(style));
  }

  _reinit() {
    this._removeClones()
    this._init()
  }

  _removeClones() {
    this.clones.forEach($clone => $clone.remove())
    this.clones = []
  }

  _bindEvents() {
    window.addEventListener(RESIZE_EVENT, this._reinit.bind(this))
  }
}

module.exports = ScrollingText
