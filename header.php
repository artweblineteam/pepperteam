<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pepperteam
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

    <title><?php bloginfo('name'); ?></title>
   
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/build//images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php  echo get_stylesheet_directory_uri(); ?>/build//images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php  echo get_stylesheet_directory_uri(); ?>/build/images/favicon-16x16.png">
    <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <link src="<?php echo get_stylesheet_directory_uri() ?>/source/styles/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/build/styles/app.css?v=1">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header>


	</header><!-- #masthead -->

	<div id="content" class="site-content">
